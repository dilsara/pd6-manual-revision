export class PD3ReportsConfig {

    pd3ReportsConfig: object[];

    getPD3ReportsConfig(userRole: string) {
        if (userRole == "POSTAL") {
            this.pd3ReportsConfig = [
                {
                    "reportName": "Postal Report",
                }
            ]
        }
    }
}