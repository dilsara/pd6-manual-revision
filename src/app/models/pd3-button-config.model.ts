export class PD3ButtonConfigModel {
    history: boolean;
    more: boolean;
    receive: boolean;
    editPension: boolean;
    rePrint: boolean;
    select: boolean;
    viewDetails: boolean;
    printInterviewLetter: boolean;
    rejectApplication: boolean;
    selectField: boolean;
    bankBranchField: boolean;
    gratuityAccept: boolean;
    bulkAccept: boolean;
    printReport: boolean;

    constructor() {
        this.history = false;
        this.more = false;
        this.receive = false;
        this.editPension = false;
        this.rePrint = false;
        this.select = false;
        this.viewDetails = false;
        this.printInterviewLetter = false;
        this.rejectApplication = false;
        this.selectField = false;
        this.bankBranchField = false;
        this.gratuityAccept = false;
        this.bulkAccept = false;
        this.printReport = false;
    }

    getButtonConfiguration(userRole: string, state: string) {
        if (userRole == "POSTAL") {
            if (state == "pending") {
                this.history = true;
                this.receive = true;
            } else if (state == "rejected") {
                this.history = true;
                this.receive = true;
            } if (state == "corrected") {
                this.history = true;
                this.receive = true;
            }
        }
        else if (userRole == "DATA_ENTRY_OFFICER") {
            if (state == "pending") {
                this.history = true;
                this.more = true;
                this.rejectApplication = true;
            } else if (state == "rejected") {
                this.history = true;
                this.more = true;
                this.rejectApplication = true;
            } else if (state == "corrected") {
                this.history = true;
                this.more = true;
                this.rejectApplication = true;
            }
        }
        else if (userRole == "SATHKARA_OFFICER") {
            if (state == "pending") {
                this.history = true;
                this.more = true;
                this.editPension = true;
                this.rePrint = true;
            } else if (state == "rejected") {
                this.history = true;
                this.more = true;
                this.rePrint = true;
            } else if (state == "corrected") {
                this.history = true;
                this.more = true;
                this.rePrint = true;
            }
        }

        else if (userRole == "PRINT_LETTER") {
            if (state == "pending") {
                this.select = true;
                this.viewDetails = true;
                this.printInterviewLetter = true;
            } else if (state == "rejected") {
                this.select = true;
                this.viewDetails = true;
                this.printInterviewLetter = true;
            } else if (state == "corrected") {
                this.select = true;
                this.viewDetails = true;
                this.printInterviewLetter = true;
            }
        }

        else if (userRole == "REGISTRATION_AD") {
            this.selectField = true;
            this.bulkAccept = true;
            this.printReport = true;

        }

        else if (userRole == "ACCOUNT_CLERK") {
            this.bankBranchField = true;
            this.gratuityAccept = true;
        }

        else if (userRole == "GRATUITY_ACCOUNTANT") {
            this.bankBranchField = true;
            this.gratuityAccept = true;
        }


    }
}