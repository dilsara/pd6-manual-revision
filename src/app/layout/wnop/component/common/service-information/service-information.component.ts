import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationMessages } from 'src/app/models/validator.message';
import { MasterDataService } from 'src/app/services/master-data.service';
import { LoggerService } from 'src/app/services/logger.service';
import { InstituteModel } from '../../../model/Institute.model'; 
import { ServiceInfoModel } from '../../../model/service_info.model';
import { IdNameModel } from '../../../model/id_nameModel';

@Component({
  selector: 'app-service-information',
  templateUrl: './service-information.component.html',
  styleUrls: ['./service-information.component.scss']
})
export class ServiceInformationComponent implements OnInit {

  form: FormGroup; 
  isClickNext: Boolean;
  services = [];
  grades = [];
  designations : IdNameModel[];
  SalaryScales = [];
  Institutes : InstituteModel[];

  constructor(
    private formBuilder: FormBuilder,
    private validationMessages: ValidationMessages,
    private mdService: MasterDataService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.isClickNext = false;

    this.form = this.formBuilder.group({
      wnop_number: [''],
      institute: [null, [Validators.required,  Validators.maxLength(300)]],
      designation: [null, Validators.required], 
      service: [null, Validators.required],
      salary_code: [null, Validators.required],
      fist_appointment_date: ['', Validators.required],
      appointment_date: ['', Validators.required],
      service_grade: ['', Validators.required],
      //salary: [''],     
    }); 

    this.initFormChangeHooks();
  }

  getModel(): ServiceInfoModel {
    return this.form.value;
  }

  setModel(model: ServiceInfoModel)  {
    this.form.patchValue(model);
  }

  get Form(): FormGroup {
    return this.form;
  }

  initFormChangeHooks() {

    //Get Designation list to UI 
    this.mdService.getDesignationsList().subscribe(
      response => { 
        this.designations = response;
      }, error => this.logger.log(error)
    );

    //Get Service list to UI 
    this.mdService.getServiceList().subscribe(
      response => { 
        this.services = response;
      }, error => this.logger.log(error)
    );

    //Get Salary Code list to UI 
    this.mdService.getSalaryScaleList().subscribe(
      response => { 
        this.SalaryScales = response;
      }, error => this.logger.log(error)
    );

    //Get Institute Code list to UI 
    this.mdService.getInstituteList().subscribe(
      response => { 
        this.Institutes = response; 
      }, error => this.logger.log(error)
    );

    //Get Grade Code list to UI 
    this.mdService.getGradeList().subscribe(
      response => { 
        this.grades = response; 
      }, error => this.logger.log(error)
    );

  }
  
  get fullName() { return this.form.get('fullName') }
  get addressLine1() { return this.form.get('addressLine1') }
  get addressLine2() { return this.form.get('addressLine2') }
  get addressLine3() { return this.form.get('addressLine3') }
  get name() { return this.form.get('name') }  

}
