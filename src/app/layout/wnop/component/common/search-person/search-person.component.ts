import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-search-person',
  templateUrl: './search-person.component.html',
  styleUrls: ['./search-person.component.scss']
})
export class SearchPersonComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder, 
    private dialogRef: MatDialogRef<SearchPersonComponent>,
  ) { }

  ngOnInit() {

    this.form = this.formBuilder.group({
      value : ['',Validators.required],
      searchBy: ['id', Validators.required],
    });
  }

  close() {
    this.dialogRef.close();
  }

  saveAndClose() {
    if(this.form.valid) {
      this.dialogRef.close(this.form);
    
    } else {
      //this.alert.show('Please Fill the Form Correctly');
    }
  }

}
