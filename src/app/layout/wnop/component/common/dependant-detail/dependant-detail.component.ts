import { Component, OnInit } from '@angular/core';
import { DependantsInfoModel } from '../../../model/dependant_info.model';
import { DependentsInfoTblModel, SpouseInfoTblModel } from '../../../model/profileModel';

@Component({
  selector: 'app-dependant-detail',
  templateUrl: './dependant-detail.component.html',
  styleUrls: ['./dependant-detail.component.scss']
})
export class DependantDetailComponent implements OnInit {

  dependantModel :DependentsInfoTblModel[]=[] ; 
  spouseModel : SpouseInfoTblModel[] =[]; 
  maritalStatus: number = -1;

  constructor() { }

  ngOnInit() {
  }

  setModel(dependantModel: DependentsInfoTblModel[],spouseModel : SpouseInfoTblModel[], maritalStatus :number) {
    this.dependantModel = dependantModel;
    this.spouseModel = spouseModel;
    this.maritalStatus = maritalStatus;
  }
}
