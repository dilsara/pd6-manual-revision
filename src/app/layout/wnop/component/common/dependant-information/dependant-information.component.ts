import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationMessages } from 'src/app/models/validator.message';
import { SpouseInfoTblModel } from '../../../model/spouse_infoTb.model';
import { MatDialog } from '@angular/material';
import { AddSpouseComponent } from '../add-spouse/add-spouse.component';
import { DependentsInfoTblModel } from '../../../model/dependant_infoTb.model';
import { AddDependantComponent } from '../add-dependant/add-dependant.component';
import { DependantsInfoModel } from '../../../model/dependant_info.model';

@Component({
  selector: 'app-dependant-information',
  templateUrl: './dependant-information.component.html',
  styleUrls: ['./dependant-information.component.scss']
})
export class DependantInformationComponent implements OnInit {
  DIALOG_WIDTH = '600px';
  spouses: SpouseInfoTblModel[] = [];
  dependents: DependentsInfoTblModel[] = [];
  
  form: FormGroup; 
  isClickNext: Boolean;
  isMarried: Boolean; 

  constructor(
    private formBuilder: FormBuilder,
    private validationMessages: ValidationMessages,
    private dialogMgr: MatDialog
  ) { }

  ngOnInit() {
    this.isClickNext = false;
    this.isMarried = false;

    this.form = this.formBuilder.group({
      marital_status: [0, Validators.required]  
    }); 

    this.initFormChangeHooks();
  }

  addDependent() {
    this.dialogMgr
      .open(AddDependantComponent, {
        panelClass: "custom-dialog-container",
        data: this.spouses
      })
      .afterClosed()
      .subscribe((response: FormGroup) => {
        if (response && response.valid) {
          this.dependents.push(JSON.parse(JSON.stringify(response.value)));
        }
      });
  }

  initFormChangeHooks() {
    //Update DS list when change district
    this.marital_status.valueChanges.subscribe(newValue => { 
      if (newValue == 0) {
        this.spouses.splice(0)
      }
    });
  }

  addSpouse() {
    this.dialogMgr
    .open(AddSpouseComponent, {
      width: this.DIALOG_WIDTH,
      panelClass: "custom-dialog-container",
    })
    .afterClosed()
    .subscribe((response: FormGroup) => {
      if(response && response.valid) {
        this.spouses.push(JSON.parse(JSON.stringify(response.value)));
      }
    })
  }

  removeDependant(row) {
    this.dependents.splice(row, 1);
  }

  removeSpouse(row) {
    this.spouses.splice(row, 1);
  }

  /**
   * get the from model
   */
  getModel(): DependantsInfoModel {
    return {
      spouses: this.spouses,
      dependants: this.dependents,
      marital_status: this.marital_status.value
    }
  }

  setModel(model: DependantsInfoModel) {
    this.marital_status.setValue(model.marital_status);
    this.spouses = model.spouses;
    this.dependents = model.dependants; 
  }

  get marital_status() { return this.form.get('marital_status')}
  get countofSpouse() { return this.spouses.length }
}
