export interface DependentsInfoTblModel {
    fullName: string;
    relation: boolean; 
    dob: Date;
    nic: string;
    differentlyAabled: boolean;
    guardian:string;
    guardian_id :number;
    person_id :number ;
}