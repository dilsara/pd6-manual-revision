import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { NewApplicationComponent } from './component/new-application/new-application.component';
import { ReRegistrationComponent } from './component/re-registration/re-registration.component';
import { WnopDashboardComponent } from './component/dashboard/dashboard.component';
import { PrintApplicationComponent } from './component/print-application/print-application.component';
import { UpdateApplicationComponent } from './component/update-application/update-application.component';



export const routes: Routes = [
  
  { path: '', component: WnopDashboardComponent },
  { path: 'new', component: NewApplicationComponent },
  { path: 'reregistration/:value', component: ReRegistrationComponent },
  { path: 'print/:value', component: PrintApplicationComponent },
  { path: 'update/:value', component: UpdateApplicationComponent },   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WnopRoutingModule { }
