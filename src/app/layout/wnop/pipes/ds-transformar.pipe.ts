import { MasterDataService } from './../../../services/master-data.service';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'dsT' })
export class DsTransformerPipe implements PipeTransform {

    constructor(private service: MasterDataService) {}

    transform(id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            this.service.getDs(id).subscribe(
                respo => {
                    resolve(respo.name);
                },
                err => reject(err)
            );
        })
    }
}