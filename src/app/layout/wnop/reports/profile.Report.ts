import { Injectable } from "@angular/core"; 
import { MasterDataService } from "src/app/services/master-data.service";
import {forkJoin} from "rxjs";

@Injectable()
export class ProfileReport {

    private printData;
    private windowOptions = 'resizable=0,width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no';

    private institute;
    private designation; 

    constructor(
        private service: MasterDataService
    ) {

    }

    print(model: any) { 
        let getInstitute = this.service.getInstitute(model.serviceInfo.institute);
        let getdesignation = this.service.getDesignations(model.serviceInfo.designation); 

        forkJoin([getInstitute,getdesignation]).subscribe(results => { 
            this.institute = results[0].name;
            this.designation = results[1].name; 

            this.printData = this.prepareReport(model);
            let popupWinindow = window.open('', '_blank', this.windowOptions);
            popupWinindow.document.open();
            popupWinindow.document.write(this.printData);
            popupWinindow.document.close();
        }, err => {

        });

        
    }

    private prepareReport(model: any): String {
    let printData = `
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>SOURCE DOCUMENT | W&OP</title>
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/qrcode.js"></script>
        <style>
            body {
                font-size: 16px;
                font-family: "Times New Roman", Times, serif;
            }
            
            h2 {
                font-size: 22px;
                margin: 0px;
                padding: 6px;
            }
            
            @media print {
                h3.heding {
                    background: black;
                    color: white;
                    padding: 6px;
                }
            }
            
            table {
                border-collapse: collapse;
                table-layout: fixed;
            }
            
            table th {
                background: #737373;
                color: white;
                text-align: left;
                padding: 3px;
                border: 1px solid #cfcfcf;
            }
            
            table td {
                color: #202020;
                text-align: left;
                padding: 3px;
                border: 1px solid #cfcfcf;
            }
            
            td.right,
            th.right {
                text-align: right !important;
            }
            
            .without-border td {
                border: none;
            }
            
            td.top,
            th.top {
                vertical-align: top;
            }
            
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
            
            @media print {
                .braker {
                    page-break-after: always;
                }
            }
        </style>
    </head>
    <!--  onload="window.print();window.close()" -->
    
    <body>
        <div style="width: 1000px; margin: 0 auto; margin-top: 5px;">
            <div style="width: 1000px; margin: 0 auto">
                <div id="qrcode" style="width:100px; height:100px; margin-top:15px; float: left;"></div>
                <p style="float: left;margin-top: 120px; margin-left: -100px;">Ref NO: ${model.personalInfo.id}</p>
                <img height="100" width="100" style="float: right; " src="http://www.pensions.gov.lk/templates/pensionsite/img/penlogo.png" alt="">
                <img height="100" width="71" style="float: right;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Emblem_of_Sri_Lanka.svg/2000px-Emblem_of_Sri_Lanka.svg.png" alt="">
                <h1 align="center">DEPARTMENT OF PENSIONS</h1>
                <h2 style="margin-top: -15px" align="center">විශ්‍රාම වැටුප් දෙපාර්තමේන්තුව</h2>
                <br>
                <h3 align="left" style="padding-left:90px">Widows'/Widowers and Orphans Pension Scheme-Application Judical and Civil Officers</h3>
            </div>
            <br> 
            <!-- ---------------------personal information ------- -->
            <h3 class="heding" align="left" style="background: black;color: white;">PERSONAL INFORMATION</h3> 
            <table class="without-border" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td valign="top" width="30%">Title</td>
                        <td>${model.personalInfo.title}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">Full Name</td>
                        <td>${model.personalInfo.fullName}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">NIC Number</td>
                        <td>${model.personalInfo.nic}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">Birth Date</td>
                        <td>${model.personalInfo.dob}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">Fist Appoinment Date</td>
                        <td>${model.serviceInfo.fist_appointment_date}</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">Working Institute</td>
                        <td>${this.institute }</td>
                    </tr>
                    <tr>
                        <td valign="top" width="30%">Designation</td>
                        <td>${this.designation}</td>
                    </tr>     
                </tbody>
            </table>
            <br>
            <br>
            <!-- ------------------------------------------------------->    
                
            <h3 class="heding" align="left">DECLARATION OF THE CONTRIBUTOT</h3>
            <hr>
            <p>I hereby declare that I have read the remark included in the widows’ and orphans’, pension fund ordinance
                No. 1 1898/ widowers’ and orphans’ pension fund ordinance No 24 and 1983 and its amendments. It is also
                confirmed that a statement similar to this was not made by me for registration under the W&OP system before
                and that the particulars mentioned above are true and correct.</p>
            <br>
            <br> 
            <table class="without-border" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left">.................................................</td>
                    <td class="right">................................................</td>
                </tr>
                <tr>
                    <td class="left">DATE</td>
                    <td class="right">SIGNATURE</td>
                </tr>
            </table>

            <br>
            <h3 class="heding" align="left">DECLARATION OF THE SUBJECT OFFICER</h3>
            <hr>
            <p>I hereby certify that all the information provided in W&OP online application was personally checked by me
            and accurate according to the personal file of the applicant.</p>
            <br>
            <br> 
            <table class="without-border" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left">.................................................</td>
                    <td class="right">................................................</td>
                </tr>
                <tr>
                    <td class="left">DATE</td>
                    <td class="right">SIGNATURE</td>
                </tr>
            </table>

            <br>
            <h3 class="heding" align="left">DECLARATION OF THE HEAD OF INSTITUTE</h3>
            <hr>
            <p>I hereby certify that all the information provided by the applicant was personally checked by me with all the
                supporting documents and is accurate accordingly. Also I certify that the applicant is eligible for Widows'/
                Widowers and Orphans Pension Scheme. Also I confirm contribution for W&OP has been deducted from the.
                employees' salary.
            </p>
            <br>
            <br> 
            <table class="without-border" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td  class="left" >Name</td>
                    <td >...........................................................................</td>
                    <td></td>
                    <td class="right"></td>
                </tr>
                <tr>
                    <td  class="left">Designation</td>
                    <td >...........................................................................</td>
                    <td></td>
                    <td class="right"></td>
                </tr>  
            </table>
            <br>
            <table class="without-border" width="100%" cellspacing="0" cellpadding="0"> 
                <tr>
                    <td class="left">.................................................</td>
                    <td class="right">................................................</td>
                </tr>
                <tr>
                    <td class="left">DATE</td>
                    <td class="right">SIGNATURE</td>
                </tr>
            </table>
            <br>
            <br>
    
            
        </div>
    </body>
    <script>
                var i = '${model.personalInfo.id}'
                console.log(i)
                    var qrcode = new QRCode(document.getElementById("qrcode"), {
                        width : 100,
                        height : 100
                    });
                    //i = 'dinesh'
                    console.log(i);
                    qrcode.makeCode(i);
                </script>   
    </html>
            `;
    
            return printData;
        }

}