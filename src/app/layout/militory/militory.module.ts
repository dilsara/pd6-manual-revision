import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IdentificationComponent } from './militory-components/identification/identification.component';
import { PersonalInformationComponent } from './militory-components/personal-information/personal-information.component';
import { SearchPersonComponent } from './militory-components/search-person/search-person.component';
import { MainComponent } from './militory-components/main/main.component';
import { ServiceInformationComponent } from './militory-components/service-information/service-information.component';
import { DependentInformationComponent } from './militory-components/dependent-information/dependent-information.component';

import { MilitoryRoutingModule } from './militory-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import { from } from 'rxjs';
import { BankInformationComponent } from './militory-components/bank-information/bank-information.component';
import { ViewDependentComponent } from './militory-components/dependent-information/view-dependent/view-dependent.component';
import { PensionInformationComponent } from './militory-components/pension-information/pension-information.component';
import { PaymentDetailsComponent } from './militory-components/payment-details/payment-details.component';
import { SubmittedComponent } from './militory-components/submitted/submitted.component';
import { AddSpouseComponent } from './militory-components/dependent-information/add-spouse/add-spouse.component';
import { AddDependentComponent } from './militory-components/dependent-information/add-dependent/add-dependent.component';
import { ValidationMessages } from './models/validator.message';
import { PrintDocumentComponent } from './militory-components/print-document/print-document.component';

@NgModule({
  declarations: [
    IdentificationComponent,
    PersonalInformationComponent,
    ServiceInformationComponent,
    DependentInformationComponent,
    SearchPersonComponent,
    MainComponent,
    BankInformationComponent,
    ViewDependentComponent,
    PensionInformationComponent,
    PaymentDetailsComponent,
    SubmittedComponent,
    AddDependentComponent,
    AddSpouseComponent,
    PrintDocumentComponent
  ],
  imports: [
    CommonModule,
    MilitoryRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatStepperModule,
    MatIconModule
  ],
  entryComponents: [
    ViewDependentComponent,
    AddDependentComponent,
    AddSpouseComponent
  ],
  providers: [
    ValidationMessages
],
})
export class MilitoryModule { }
