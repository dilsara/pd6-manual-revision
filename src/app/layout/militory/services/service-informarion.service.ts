import { Injectable } from '@angular/core';
import { MilitoryAPI } from 'src/app/http/militory.api';
import { ConfigService } from 'src/app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceInformarionService {

  constructor(
    private api: MilitoryAPI,
    private config: ConfigService
  ) { }

  
  /**
   * GET SALARY CODE
   */
  getSalaryCode(){
    return this.config.get(this.api.SALARY_CODES());
  }

  /**
   * GET SALARY CIRCULAR
   */
  getSalaryCircular(){
    // return salary circular list
  }

  /**
   * GET SERVICES
   */
  getServices(){
    return this.config.get(this.api.SERVICES());
  }
  
  /**
   * GET DESIGNATION
   */
  getDesignation(){
    return this.config.get(this.api.DESIGNATIONS());
  }

  /**
   * GET GRADES
   */
  getGrades(){
    return this.config.get(this.api.GRADES());
  }

}
