import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { BankModel } from '../../models/bank.model';

@Component({
  selector: 'app-bank-information',
  templateUrl: './bank-information.component.html',
  styleUrls: ['./bank-information.component.scss']
})
export class BankInformationComponent implements OnInit {

  banks = ['BOC','PB','NSB' ];
  branches = ['Mtugama', 'Kalutara','Colombo'];

  bankInfoForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.bankInfoForm = this.formBuilder.group({
      bank: ['',Validators.required],
      branch: ['', Validators.required],
      accountNo: ['', Validators.required]
    });

    this.initFormChangeHooks();
  }

  getModel(): BankModel {
    return this.bankInfoForm.value;
  }

  /**
   * initial changes
   */
  initFormChangeHooks(){

  }

}
