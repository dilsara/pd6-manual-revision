import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonalInformationComponent } from '../personal-information/personal-information.component';
import { ServiceInformationComponent } from '../service-information/service-information.component';
import { DependentInformationComponent } from '../dependent-information/dependent-information.component';
import { BankInformationComponent } from '../bank-information/bank-information.component';
import { PensionInformationComponent } from '../pension-information/pension-information.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {


  @ViewChild(PersonalInformationComponent) personalInformationForm: PersonalInformationComponent;
  @ViewChild(ServiceInformationComponent) serviceInformationForm: ServiceInformationComponent;
  @ViewChild(DependentInformationComponent) dependentInformationForm: DependentInformationComponent;
  @ViewChild(BankInformationComponent) bankInformationForm: BankInformationComponent;
  @ViewChild(PensionInformationComponent) pensionInformationForm: PensionInformationComponent;

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  forthFormGroup: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.forthFormGroup = this._formBuilder.group({
      forthCtrl: ['', Validators.required]
    });
    // console.log(this.personalInformationForm.getModel());
  }

  submitBtn() {
    // console.log(this.personalInformationForm.getModel());

    // let profile = {
    //   personalInfo: this.personalInformationForm.getModel(),
    //   serviceInfo: this.serviceInformationForm.getModel(),
    //   dependentInfo: this.dependentInformationForm.getModel(),
    //   bankInfo: this.bankInformationForm.getModel(),
    //   pensionInfo: this.pensionInformationForm.getModel()
    // }

    console.log(this.getModel());

    
    this.router.navigateByUrl("/militory/submit");
  }

  getModel() {
    let profile = {
      personalInfo: this.personalInformationForm.getModel(),
      serviceInfo: this.serviceInformationForm.getModel(),
      dependants: this.dependentInformationForm.getModel().dependants,
      spouses: this.dependentInformationForm.getModel().spouses,
      marital_status: this.dependentInformationForm.getModel().marital_status,
      bankInfo: this.bankInformationForm.getModel(),
      pensionInfo: this.pensionInformationForm.getModel()
    }

    return profile;
  }


}
