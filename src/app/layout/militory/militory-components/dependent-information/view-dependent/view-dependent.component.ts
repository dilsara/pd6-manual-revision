import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-view-dependent',
  templateUrl: './view-dependent.component.html',
  styleUrls: ['./view-dependent.component.scss']
})
export class ViewDependentComponent implements OnInit {

  dependentDetails = [
    {
      dependentName: 'Saman',
      relationship: 'Son',
      dependentNic: '882892805V',
      dependentDob: '1988-12-12',
      dependentAwardNo: 27,
      dependentAddress: 'Kalutara',
      dependentStatus: 'Disabled',
      dependentMaritialStaus: 'Single',
      dependentBank: 'BOC',
      dependentBranch: 'Kalutara',
      dependentAccNo: 25823654
    }
  ];

  moreDependentInfoForm: FormGroup;
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ViewDependentComponent>,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      dependentName:['Saman',Validators.required],
      relationship: ['Daughter',Validators.required],
      dependentNic: ['882892805V',Validators.required],
      dependentDob: ['1988-12-12',Validators.required],
      dependentAwardNo: [50,Validators.required],
      dependentAddress: ['Kalutara',Validators.required],
      dependentStatus: ['Disabled',Validators.required],
      dependentMaritialStaus: ['Single',Validators.required],
      dependentBank: ['BOC',Validators.required],
      dependentBranch: ['Kalutara',Validators.required],
      dependentAccNo: [25823654,Validators.required]
    });
    this.moreDependentInfoForm = new FormGroup({
      dependentName: new FormControl(),
      dependentNic: new FormControl()
    });

    this.initFormChangeHooks();
  }

  /**
   * initial changes
   */
  initFormChangeHooks(){

  }

  /**
   * Windows Close
   */
  close() {
    this.dialogRef.close();
  }


}
