import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonalInformationService } from '../../services/personal-information.service';
import { getDefaultService } from 'selenium-webdriver/opera';
import { MilitoryPersonModel } from '../../models/personal-infomation.model';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {

  salutations = ['Mr.', 'Mrs.', 'Miss.'];

  organizations = ['Army', 'Navy', 'Air Force', 'Police'];
  types = ['Commissioned', 'Non-Commissioned', 'Civil'];
  regiments = ['A', 'B', 'C'];
  genders = ['Male', 'Female'];
  maritialStatuses = ['Married', 'Unmarried'];
  districts: any;
  dss: any;
  gnDivisions: any;
  // gnDivisions = ['Maradana', 'Matugama', 'Agalawatta'];
  statuses = ['Deceased', 'Disabled'];
  // dss = ['Giribawa', 'Galgamuwa', 'Ehetuwewa', 'Ambanpola'];

  personalInfoForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private personalInfoService: PersonalInformationService
  ) { }

  ngOnInit() {
    this.personalInfoForm = this.formBuilder.group({
      salutation: [''],
      fullName: [''],
      // preferedName: ['', Validators.required],
      dob: [''],
      doAgeFiftyFive: [''],
      nic: [''],
      gender: [''],
      permenantAddress: this.formBuilder.group({
        addressLine1: [''],
        addressLine2: [''],
        addressLine3: ['']
      }),
      telephone: [''],
      mobile: [''],
      email: [''],
      status: [''],
      maritialStatus: [''],
      district: [''],
      ds: [''],
      gnDivision: ['']
    });

    // this.form = this.formBuilder.group({
    //   salutation: ['', Validators.required],
    //   fullName: ['', Validators.required],
    //   // preferedName: ['', Validators.required],
    //   dob: ['', Validators.required],
    //   doAgeFiftyFive: ['', Validators.required],
    //   nic: ['', Validators.required],
    //   gender: ['', Validators.required],
    //   permenantAddress: this.formBuilder.group({
    //     addressLine1: ['', Validators.required],
    //     addressLine2: [''],
    //     addressLine3: ['']
    //   }),
    //   telephone: ['', Validators.required],
    //   mobile: ['', Validators.required],
    //   email: ['', Validators.required],
    //   status: ['', Validators.required],
    //   maritialStatus: ['', Validators.required],
    //   district: ['', Validators.required],
    //   ds: ['', Validators.required],
    //   gnDivision: ['', Validators.required]
    // });

    this.getDistricts();

    this.initFormChangeHooks();
  }

  getModel(): MilitoryPersonModel {
    return this.personalInfoForm.value;
  }

  get Form() {
    return this.personalInfoForm;
  }

  /**
   * initial changes
   */
  initFormChangeHooks() {
    this.District.valueChanges.subscribe(newValue => {
      // console.log(this.personalInfoForm.get('district').value);
      let district_id = this.personalInfoForm.get('district').value;
      this.getDistrictSecretariat(district_id);
    });
    this.Ds.valueChanges.subscribe(newValue => {
      // console.log(this.personalInfoForm.get('ds').value);
      let ds_id = this.personalInfoForm.get('ds').value;
      this.getGnDivision(ds_id);
    });
    // this.Ds.valueChanges.subscribe(newValue=>{
    //   console.log(this.personalInfoForm.get('district').value);

    // });
  }

  /**
   * proceed button
   */
  personalInfoProceed() {
    this.router.navigateByUrl("/militory/service-information");
    console.log('Personal Information Proceed');
  }


  /**
   * previous button
   */
  personalInfoPrevious() {
    this.router.navigateByUrl("/militory/identification");
  }

  /**
   * get districts
   */
  getDistricts() {
    this.personalInfoService.getDistrictList().subscribe(data => {
      this.districts = data;
    }, error => {
      console.log("District List not found. ", error);
    });

  }

  /**
   * get district secretariat id
   * @param district_id 
   */
  getDistrictSecretariat(district_id) {
    this.personalInfoService.getDsList(district_id).subscribe(data => {
      this.dss = data;
    }, error => {
      console.log("DS list not found. " + error);
    });
    // this.personalInfoForm.get('gnDivision');
  }

  /**
   * get grama niladari divisions
   * @param ds_id 
   */
  getGnDivision(ds_id) {
    this.personalInfoService.getGnDivision(ds_id).subscribe(data => {
      this.gnDivisions = data;
    }, error => {
      console.log("Gn Division List Not Found. ", error);
    });
  }

  get District() { return this.personalInfoForm.get('district') }
  get Ds() { return this.personalInfoForm.get('ds') }
  get GnDivision() { return this.personalInfoForm.get('gnDivision') }


}
