import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionInformationComponent } from './pension-information.component';

describe('PensionInformationComponent', () => {
  let component: PensionInformationComponent;
  let fixture: ComponentFixture<PensionInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PensionInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
