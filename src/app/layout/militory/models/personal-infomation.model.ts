import { Address } from "./address.model";

export class MilitoryPersonModel {
    salutation: string;
    fullName: string;
    // preferedName: string;
    dob: string;
    doAgeFiftyFive: string;
    nic: string;
    gender: string;
    permenantAddress: Address;
    telephone: string;
    mobile: string;
    email: string;
    status: string;
    maritialStatus: string;
    district: string;
    ds: string;
    gnDivision: string;
}