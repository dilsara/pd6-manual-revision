import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Pd3ListViewComponent } from './pd3-list-view/pd3-list-view.component';
import { ViewPensionsComponent } from './view-pensions/view-pensions.component';
import { Pd3DetailedViewComponent } from './pd3-detailed-view/pd3-detailed-view.component';
import { EditPensionsComponent } from './components/edit-pensions/edit-pensions.component';
import { ViewGratuitiesComponent } from './components/view-gratuities/view-gratuities.component';
import { RejectionPanelComponent } from './components/rejection-panel/rejection-panel.component';
import { PostalReportComponent } from './reports/postal-report/postal-report.component';
import { ReportMainViewComponent } from './reports/report-main-view/report-main-view.component';
import { AwardPrintComponent } from './components/award-print/award-print.component';
import { QuickAccessComponent } from './components/quick-access/quick-access.component';
import {SathkaraDailyReportComponent} from './reports/daily-report-sathkara/daily-report-sathkara.component'
import { DailyGratuityReportComponent } from './reports/daily-gratuity-report/daily-gratuity-report.component';
import { DailyRegistrationRecheckComponent } from './reports/daily-registration-recheck-report/daily-registration-recheck.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'pd3-list-view/:state', component: Pd3ListViewComponent },
  { path: ':state/pd3-view-pensions/:refNumber', component: ViewPensionsComponent },
  { path: ':state/pd3-detailed-view/:refNumber/:pensionNumber', component: Pd3DetailedViewComponent },
  { path: 'pd3-view-gratuities', component: ViewGratuitiesComponent },
  { path: 'edit-pension', component: EditPensionsComponent },
  { path: ':state/rejection/:refNumber/:pensionNumber', component: RejectionPanelComponent },
  { path: 'reports/postal-report/:date', component: PostalReportComponent },
  { path: 'reports', component: ReportMainViewComponent },
  { path: 'reports/sathkara_daily',component:SathkaraDailyReportComponent},
  { path: 'award', component: AwardPrintComponent },
  { path: 'quick-access', component: QuickAccessComponent },
  { path: 'reports/gratuity_daily', component: DailyGratuityReportComponent },
  { path: 'reports/daily-registration-recheck', component: DailyRegistrationRecheckComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pd3PensionsRoutingModule { }
