export class Calculations {
    total: number;
    total2020: number;
    reducedSalary: number;
    unreducedSalary: number;
    reducedSalary2020: number;
    unreducedSalary2020: number;
    totalAllowance: number;

    constructor() {
        this.total = 0.00;
        this.total2020 = 0.00;
        this.reducedSalary = 0.00;
        this.unreducedSalary = 0.00;
        this.reducedSalary2020 = 0.00;
        this.unreducedSalary2020 = 0.00;
        this.totalAllowance = 0.00;
    }
}