export class PD3DependentModel {
    id: number;
    name: string;
    address: string;
    nic: string;
    maritalStatus: boolean;
    relation: string;
    dob: string;
    merrageDate: string;
    contactNumber: string;
    disabled: boolean;

    constructor() {
        this.id = 0;
        this.name = "";
        this.address = "";
        this.nic = "";
        this.maritalStatus = false;
        this.relation = "";
        this.dob = "";
        this.merrageDate = "";
        this.contactNumber = "";
        this.disabled = false;
    }

}