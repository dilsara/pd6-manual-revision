export class PostalReport {
    name: string;
    nic: string;
    pensionPoint: string;
    refNumber: number;

    constructor() {
        this.name = "";
        this.nic = "";
        this.pensionPoint = "";
        this.refNumber = 0;
    }
}