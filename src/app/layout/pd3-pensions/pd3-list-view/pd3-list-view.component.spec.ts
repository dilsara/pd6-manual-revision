import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd3ListViewComponent } from './pd3-list-view.component';

describe('Pd3ListViewComponent', () => {
  let component: Pd3ListViewComponent;
  let fixture: ComponentFixture<Pd3ListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd3ListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd3ListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
