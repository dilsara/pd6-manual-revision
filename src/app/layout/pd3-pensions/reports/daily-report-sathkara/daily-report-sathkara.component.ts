import { Component, OnInit } from '@angular/core';
import { DataConfigService } from '../../services/data-config.service';
import { ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'app-sathkara-daily-report',
  templateUrl: './daily-report-sathkara.component.html',
  styleUrls: ['./daily-report-sathkara.component.scss']
})
export class SathkaraDailyReportComponent implements OnInit {

  public date: string;
  public fromDate;
  public toDate;
  public iframeURL :SafeResourceUrl;
  public iframeLoaded:boolean=false;
  public iframeclass="onframeload";
  public loaded=false;
  public loadedCount=0;
  constructor(
    public dataConfig: DataConfigService,
    private activatedRoute: ActivatedRoute,
    private sanitizer:DomSanitizer
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.date = params["date"]
    })
  }

   viewReport(){
     this.iframeLoaded=true;
    $('iframe').attr('src', $('iframe').attr('src'));
    this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(`http://192.168.100.3:81/jasperserver/rest_v2/reports/reports/daily_report_from_sathkara_for_all.html?SELECTED_DATE=${this.fromDate}`);
    $('iframe').attr('src', $('iframe').attr('src'));
    this.loaded=true;
  }
  
  download(){
    window.open(`http://192.168.100.3:81/jasperserver/rest_v2/reports/reports/daily_report_from_sathkara_for_all.pdf?SELECTED_DATE=${this.fromDate}`,'this');
  }
  
  changeStyle(){
      this.iframeclass="afterframeload";
   }
 
   onMyFrameLoad() {
     this.loadedCount+=1;
     if(this.loadedCount==2){
       this.changeStyle();
       this.loadedCount=0;
     }    
  }
}
