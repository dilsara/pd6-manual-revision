import { Component, OnInit } from '@angular/core';
import { PD3ReportsConfig } from 'src/app/models/pd3-reports-config.model';
import { MatDialog } from '@angular/material';
import { PostalDatepickerComponent } from '../../popups/postal-datepicker/postal-datepicker.component';
import { Pd3ReportsServiceService } from '../../services/pd3-reports-service.service';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-main-view',
  templateUrl: './report-main-view.component.html',
  styleUrls: ['./report-main-view.component.scss']
})
export class ReportMainViewComponent implements OnInit {

  reportConfig: PD3ReportsConfig;

  public postalReport:boolean=false;
  public sathkaraReport:boolean=false;
  public dailyGratuityReport:boolean=false;
  public dailyRegistrationRecheckReport:boolean=false;

  constructor(
    private dialog:MatDialog,
    private reportService:Pd3ReportsServiceService,
    private router: Router
  ) {
    this.reportConfig = new PD3ReportsConfig();
  }

  ngOnInit() {
    this.reportConfig.getPD3ReportsConfig(localStorage.getItem("userRole"));
    this.customizeReportMenu();
  }

    //popup for postal datepicker
    postalDatepickerPopup(): void {
      const dialogRef = this.dialog.open(PostalDatepickerComponent, {
        width: '450px',
        height: '200px',
       
      });
  
      dialogRef.afterClosed().subscribe(result => {
      });
    }

    viewReport(){
      // this.reportService.authenticate().subscribe(
      //   data=>{
      //     console.log(data);
      //   },
      //   err=>{
      //     console.log(err);
      //   }
      // )

      // let content ='http://192.168.100.101:85/jasperserver/rest_v2/reports/reports/DataSources/postal_report_New.html';
      // console.log(content);

      // $('mydiv').load('http://192.168.100.101:85/jasperserver/rest_v2/reports/reports/DataSources/postal_report_New.html');
      // console.log(document.getElementById('mydiv'));

      // $.get("http://192.168.100.101:85/jasperserver/rest_v2/reports/reports/DataSources/postal_report_New.html", function(data, status){
      //   alert("Data: " + data + "\nStatus: " + status);
      // });
      // document.getElementById("mydiv").innerHTML='<object type="text/html" data="http://192.168.100.101:85/jasperserver/rest_v2/reports/reports/DataSources/postal_report_New.html" ></object>';

      // window.open('http://192.168.100.101:85/jasperserver/rest_v2/reports/reports/daily_report_from_sathkara_for_all.pdf?SELECTED_DATE=2019-03-01&CURRENT_DATE=2019-03-01');
      // window.open('http://192.168.100.101:81/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=/Reports/daily_report_from_sathkara_for_all&SELECTED_DATE=2019-03-05&CURRENT_DATE=2019-03-05&output=pdf')
      // document.getElementById("mydiv").innerHTML="heekkkl";
    }

    viewSathkaraReport(){
      this.router.navigate(['/pd3-pensions/reports/sathkara_daily']);
    }

    viewDailyGratuityReport(){
      this.router.navigate(['/pd3-pensions/reports/gratuity_daily']);
    }

    viewDailyRegistrationRecheckReport(){
      this.router.navigate(['/pd3-pensions/reports/daily-registration-recheck']);
    }

    customizeReportMenu(){
      let userRole=localStorage.getItem("userRole");
      switch (userRole) {
        case "DATA_ENTRY_OFFICER":
          this.sathkaraReport=true;
          this.dailyGratuityReport=true;
          this.dailyRegistrationRecheckReport=true;
          break;
        case "POSTAL":
          this.postalReport=true;
          break;
        case "SATHKARA_OFFICER":
            this.sathkaraReport=true;
            break;
        default:
          break;
      }
    }
}
