import { Component, OnInit } from '@angular/core';
import PD3DashboardConfig from '../pd3-config/pd3-dashboard.config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  dashboardConfig: PD3DashboardConfig = new PD3DashboardConfig();


  constructor(
  ) { }

  ngOnInit() {
    this.dashboardConfig.getDashboardConfig(localStorage.getItem("userRole"));
  }

}
