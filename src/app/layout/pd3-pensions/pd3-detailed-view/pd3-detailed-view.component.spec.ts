import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd3DetailedViewComponent } from './pd3-detailed-view.component';

describe('Pd3DetailedViewComponent', () => {
  let component: Pd3DetailedViewComponent;
  let fixture: ComponentFixture<Pd3DetailedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd3DetailedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd3DetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
