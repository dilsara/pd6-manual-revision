import { Component, OnInit } from '@angular/core';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { Pd3GratuityService } from '../../services/pd3-gratuity.service';
import { DataConfigService } from '../../services/data-config.service';
import { ViewGratuitiesModel } from '../../models/view-gratuities.model';
import { GratuityBankDetailsModel } from '../../models/gratuity-bank-details.model';
import { PD3PensionerModel } from '../../models/pensioner.model';
import { NotificationsService } from 'src/app/services/notifications.service';
import { error } from 'util';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';
import { AcceptGratuitiesPopupComponent } from '../../popups/accept-gratuities-popup/accept-gratuities-popup.component';
import { MatDialog } from '@angular/material';
import { PendingGratuityModel } from '../../models/pending-gratuity.model';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-view-gratuities',
  templateUrl: './view-gratuities.component.html',
  styleUrls: ['./view-gratuities.component.scss']
})
export class ViewGratuitiesComponent implements OnInit {

  pd3ViewGratuity: ViewGratuitiesModel;
  buttonConfig: PD3ButtonConfigModel;
  pendingGratuityList = [];
  selectedPensioners = [];
  urlList = [];

  constructor(
    private gratuityService: Pd3GratuityService,
    public dataConfig: DataConfigService,
    private notify: NotificationsService,
    private dialog: MatDialog,
    public spinner: SpinnerService
  ) {
    this.pd3ViewGratuity = new ViewGratuitiesModel();
    this.buttonConfig = new PD3ButtonConfigModel();
  }

  ngOnInit() {
    this.getPendingGratuities();
    this.buttonConfig.getButtonConfiguration(localStorage.getItem("userRole"), "pending");
  }

  // 
  getPendingGratuities() {
    if (localStorage.getItem("userRole") == "REGISTRATION_AD") {
      this.spinner.listViewSpinner = true;
      this.gratuityService.getPendingGratuities(105)
        .subscribe(data => {
          this.dataConfig.pd3PendingGratuity = data["list"];
          this.getDetailedGratuities();
        }, error => {
          this.dataConfig.pd3PendingGratuity = new Array<PendingGratuityModel>();
        })
    }
    else if (localStorage.getItem("userRole") == "ACCOUNT_CLERK") {
      this.spinner.listViewSpinner = true;
      this.gratuityService.getPendingGratuities(405)
        .subscribe(data => {
          this.dataConfig.pd3PendingGratuity = data["list"];
          this.getDetailedGratuities();
        }, error => {
          this.spinner.listViewSpinner = false;
          this.dataConfig.pd3PendingGratuity = new Array<PendingGratuityModel>();
          this.pendingGratuityList = [];
        })
    }

  }

  getDetailedGratuities() {
    this.pendingGratuityList = [];
    this.dataConfig.pd3PendingGratuity.forEach(element => {
      // console.log(element.id.toString());
      this.gratuityService.getGratuityBankDetails(111, element.id.toString())
        .subscribe(data => {
          this.pd3ViewGratuity = new ViewGratuitiesModel();
          this.dataConfig.pd3GratuityBankDetails = new GratuityBankDetailsModel();
          this.dataConfig.pd3GratuityBankDetails = JSON.parse(JSON.stringify(data));
          console.log(this.dataConfig.pd3GratuityBankDetails.pensioner.url);
          this.pd3ViewGratuity.pensionerNumber = this.dataConfig.pd3GratuityBankDetails.pensioner.url;
          this.pd3ViewGratuity.accountNumber = this.dataConfig.pd3GratuityBankDetails.accountNumber;
          this.pd3ViewGratuity.gratuityAmount = this.dataConfig.pd3GratuityBankDetails.gratuityAmount;
          this.pd3ViewGratuity.gratuityId = this.dataConfig.pd3GratuityBankDetails.id;
          // this.getPensionerDetails(this.dataConfig.pd3GratuityBankDetails.pensioner.url);
          this.pendingGratuityList.push(this.pd3ViewGratuity);
          this.spinner.listViewSpinner = false;
        }, error => {
          this.spinner.listViewSpinner = false;
          this.pendingGratuityList = [];
        })
    })
  }

  getCheckboxes() {
    this.selectedPensioners = (this.pendingGratuityList.filter(x => x.checked === true).map(x => x.gratuityId));
    console.log(this.selectedPensioners);
  }

  // getPensionerDetails(refNo) {
  //   this.pensionService.getPd3PensionersDetails(refNo)
  //     .subscribe(data => {
  //       this.dataConfig.pd3Pensioner = new PD3PensionerModel();
  //       this.dataConfig.pd3Pensioner = JSON.parse(JSON.stringify(data));
  //       this.pd3ViewGratuity.name = this.dataConfig.pd3Pensioner.name;
  //       this.pd3ViewGratuity.nic = this.dataConfig.pd3Pensioner.nic;
  //       this.pendingGratuityList.push(this.pd3ViewGratuity);
  //     })
  // }

  acceptGratuities() {
    this.selectedPensioners.forEach(element => {
      console.log(element);
      this.pendingGratuityList = [];
      let subscription = this.gratuityService.acceptGratuities(111, element, 405)
        .subscribe(() => {
          this.getPendingGratuities();
        }, error => {
          console.log(error);
        });
      setTimeout(() => subscription.unsubscribe(), 10000);
    })
    this.notify.openSnackBar('Applications accepted successfully', '', '');
  }

  //popup view for gratuity details
  gratuityAcceptPopup(gratuityId: number): void {
    const dialogRef = this.dialog.open(AcceptGratuitiesPopupComponent, {
      width: '500px',
      height: '500px',
      data: { gratuityId: gratuityId }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getPendingGratuities();
    });
  }
}