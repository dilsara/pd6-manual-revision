import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';

@Component({
  selector: 'app-view-photo-popup',
  templateUrl: './view-photo-popup.component.html',
  styleUrls: ['./view-photo-popup.component.scss']
})
export class ViewPhotoPopupComponent implements OnInit {

  pensionerImage: string;
  covertedImage: Blob;

  constructor(
    private dialogRef: MatDialogRef<ViewPhotoPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private pensionService: Pd3PensionsService
  ) { }

  ngOnInit() {
    this.pensionService.getPensionerPhoto(this.data.referenceNumber) 
      .subscribe(data => {
        this.pensionerImage = "data:image/png;base64," + data["value"];
        this.convertImage();
      })
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  convertImage() {

    // Naming the image
    const date = new Date().valueOf();
    let text = '';
    const possibleText = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      text += possibleText.charAt(Math.floor(Math.random() * possibleText.length));
    }
    // Replace extension according to your media type
    const imageName = date + '.' + text + '.jpeg';
    // call method that creates a blob from dataUri
    const imageBlob = this.dataURItoBlob(this.pensionerImage);
    const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
    this.covertedImage = imageBlob;
  }

  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });
    return blob;
  }
}
