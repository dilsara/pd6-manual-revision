import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GratuityBankDetailsPopupComponent } from './gratuity-bank-details-popup.component';

describe('GratuityBankDetailsPopupComponent', () => {
  let component: GratuityBankDetailsPopupComponent;
  let fixture: ComponentFixture<GratuityBankDetailsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GratuityBankDetailsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GratuityBankDetailsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
