import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentInformationPopupComponent } from './dependent-information-popup.component';

describe('DependentInformationPopupComponent', () => {
  let component: DependentInformationPopupComponent;
  let fixture: ComponentFixture<DependentInformationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentInformationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentInformationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
