import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { DataConfigService } from '../../services/data-config.service';

@Component({
  selector: 'app-dependent-information-popup',
  templateUrl: './dependent-information-popup.component.html',
  styleUrls: ['./dependent-information-popup.component.scss']
})
export class DependentInformationPopupComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DependentInformationPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private pensionService: Pd3PensionsService,
    public dataConfig: DataConfigService
  ) { }

  ngOnInit() {
    this.pensionService.getPd3PensionerSingleDependent(this.data.refnumber, this.data.dependentId)
      .subscribe(data => {
        this.dataConfig.pd3Dependent = JSON.parse(JSON.stringify(data));
      })
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

}
