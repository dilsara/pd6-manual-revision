import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateChangeConfirmationPopupComponent } from './state-change-confirmation-popup.component';

describe('StateChangeConfirmationPopupComponent', () => {
  let component: StateChangeConfirmationPopupComponent;
  let fixture: ComponentFixture<StateChangeConfirmationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateChangeConfirmationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateChangeConfirmationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
