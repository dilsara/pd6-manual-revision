import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DataConfigService } from '../../services/data-config.service';

@Component({
  selector: 'app-service-details-popup',
  templateUrl: './service-details-popup.component.html',
  styleUrls: ['./service-details-popup.component.scss']
})
export class ServiceDetailsPopupComponent implements OnInit {

  
  constructor(
    private dialogRef: MatDialogRef<ServiceDetailsPopupComponent>,
    public dataConfig: DataConfigService
  ) { }

  ngOnInit() {

  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }


}
