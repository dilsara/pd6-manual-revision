import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostalDatepickerComponent } from './postal-datepicker.component';

describe('PostalDatepickerComponent', () => {
  let component: PostalDatepickerComponent;
  let fixture: ComponentFixture<PostalDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostalDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostalDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
