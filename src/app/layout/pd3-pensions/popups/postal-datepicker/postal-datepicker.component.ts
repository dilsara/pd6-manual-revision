import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pd3ReportsServiceService } from '../../services/pd3-reports-service.service';
import { DataConfigService } from '../../services/data-config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-postal-datepicker',
  templateUrl: './postal-datepicker.component.html',
  styleUrls: ['./postal-datepicker.component.scss']
})
export class PostalDatepickerComponent implements OnInit {

  inputDate: string;
  constructor(
    public dialogRef: MatDialogRef<PostalDatepickerComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private reportService: Pd3ReportsServiceService,
    private dataConfig: DataConfigService,
    private router: Router

  ) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  generate() {
    this.reportService.getPostalReport(this.inputDate)
      .subscribe(data => {
        this.dataConfig.pd3PostalList = data["log"];
        this.close();
        this.router.navigateByUrl('/pd3-pensions/reports/postal-report/' + this.inputDate);
      })
  }

}
