import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintLetterPopupComponent } from './print-letter-popup.component';

describe('PrintLetterPopupComponent', () => {
  let component: PrintLetterPopupComponent;
  let fixture: ComponentFixture<PrintLetterPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintLetterPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintLetterPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
