import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraPaymentDetailsPopupComponent } from './extra-payment-details-popup.component';

describe('ExtraPaymentDetailsPopupComponent', () => {
  let component: ExtraPaymentDetailsPopupComponent;
  let fixture: ComponentFixture<ExtraPaymentDetailsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraPaymentDetailsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraPaymentDetailsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
