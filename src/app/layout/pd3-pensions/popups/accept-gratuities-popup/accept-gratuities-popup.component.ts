import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataConfigService } from '../../services/data-config.service';
import { Pd3GratuityService } from '../../services/pd3-gratuity.service';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-accept-gratuities-popup',
  templateUrl: './accept-gratuities-popup.component.html',
  styleUrls: ['./accept-gratuities-popup.component.scss']
})
export class AcceptGratuitiesPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AcceptGratuitiesPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public dataConfig: DataConfigService,
    private gratuityService: Pd3GratuityService,
    private notify: NotificationsService
  ) { }

  ngOnInit() {
    this.gratuityService.getGratuityBankDetails(111, this.data.gratuityId)
      .subscribe(data => {
        this.dataConfig.pd3GratuityBankDetails = JSON.parse(JSON.stringify(data));
      })
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  acceptGratuity() {
    let subscription = this.gratuityService.acceptGratuities(111, this.data.gratuityId, 505)
      .subscribe(() => {
        this.dialogRef.close();
        this.notify.openSnackBar('Application accepted successfully', '', '');
      })
    setTimeout(() => subscription.unsubscribe(), 10000);
  }

}
