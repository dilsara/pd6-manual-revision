import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { DataConfigService } from '../../services/data-config.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-receive-application-popup',
  templateUrl: './receive-application-popup.component.html',
  styleUrls: ['./receive-application-popup.component.scss']
})
export class ReceiveApplicationPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ReceiveApplicationPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private pensionService: Pd3PensionsService,
    public dataConfig: DataConfigService,
    private notify: NotificationsService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  //receive  a single pension
  receivePension() {
    this.data.pensions.forEach(element => {
      this.pensionService.receivePension(this.data.id, element.id)
      .subscribe(() => {
        this.notify.openSnackBar("Pension received successfully", "", 'green');
        this.dialogRef.close();
        
        // this.router.navigateByUrl("/pd3-pensions/pd3-list-view/pending");
      },error=>{
        this.notify.openSnackBar("Application receive unsuccessfull,Please check current state of the application",'','');

      })
    });
  }

}
