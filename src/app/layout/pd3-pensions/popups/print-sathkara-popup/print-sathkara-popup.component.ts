import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-print-sathkara-popup',
  templateUrl: './print-sathkara-popup.component.html',
  styleUrls: ['./print-sathkara-popup.component.scss']
})
export class PrintSathkaraPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PrintSathkaraPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router
  ) { }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  accept() {
    this.dialogRef.close();
    this.router.navigateByUrl("/pd3-pensions/award"); 
  }

}
