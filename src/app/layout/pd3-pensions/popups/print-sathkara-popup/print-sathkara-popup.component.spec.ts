import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintSathkaraPopupComponent } from './print-sathkara-popup.component';

describe('PrintSathkaraPopupComponent', () => {
  let component: PrintSathkaraPopupComponent;
  let fixture: ComponentFixture<PrintSathkaraPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintSathkaraPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintSathkaraPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
