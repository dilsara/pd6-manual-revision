import { Injectable } from '@angular/core';
import PD3DashboardConfig from '../pd3-config/pd3-dashboard.config';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';
import { PD3ListView } from '../models/pd3-listview.model';
import { ViewPensionsModel } from '../models/view-pensions.model';
import { PD3PensionHistory } from '../models/pd3-pension-history.model';
import { PD3PensionerModel } from '../models/pensioner.model';
import { PD3BasicInformationModel } from '../models/pd3-basic-information.model';
import { PD3DependentModel } from '../models/pd3-dependent.model';
import { PD3PensionModel } from '../models/pd3-pension.model';
import { ConsolidatedSalary2020Model } from '../models/consolidated-salary.model';
import { GratuityBankDetailsModel } from '../models/gratuity-bank-details.model';
import { PendingGratuityModel } from '../models/pending-gratuity.model';
import { PostalReport } from '../models/postal-report.model';
import { ExtraPaymentDetails } from '../models/extra-paymets.model';
import { Calculations } from '../models/calculations.model';

@Injectable({
	providedIn: 'root'
})
export class DataConfigService {

	/**
	 * @author Kasun Gunathilaka
	 * pd-3 data configuration
	 */
	pd3DashboardConfig: PD3DashboardConfig = new PD3DashboardConfig(); // data configuration
	pd3ButtonConfig: PD3ButtonConfigModel = new PD3ButtonConfigModel(); //button configuration object
	pd3ListView: Array<PD3ListView> = new Array<PD3ListView>(); //pd3 list view
	pd3ViewPensions: Array<ViewPensionsModel> = new Array<ViewPensionsModel>();// pensioners all pensioners
	pd3PensionHistory: Array<PD3PensionHistory> = new Array<PD3PensionHistory>();//pd3 pension history
	pd3Pensioner: PD3PensionerModel = new PD3PensionerModel(); // pd3 pensioner
	pd3BasicInformation: PD3BasicInformationModel = new PD3BasicInformationModel(); //pd3 pensioner basic information
	pd3Dependent: PD3DependentModel = new PD3DependentModel(); //single dependent
	pd3DependentArray: Array<PD3DependentModel> = new Array<PD3DependentModel>(); //dependent array
	pd3Pension: PD3PensionModel = new PD3PensionModel(); // single pension
	pd3ConsolidatedSalary2020: ConsolidatedSalary2020Model = new ConsolidatedSalary2020Model(); // 2020 consolidated salary
	pd3GratuityBankDetails: GratuityBankDetailsModel = new GratuityBankDetailsModel(); // pd3 gratuity & bank details
	pd3PendingGratuity: Array<PendingGratuityModel> = new Array<PendingGratuityModel>(); //pending gratuity
	pd3PostalList: Array<PostalReport> = new Array<PostalReport>(); //postal report
	pd3ExtraPayments: Array<ExtraPaymentDetails> = new Array<ExtraPaymentDetails>(); // extra payment details
	pd3Calculations: Calculations = new Calculations();

	constructor() { }
}
