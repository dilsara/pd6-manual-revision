import { TestBed } from '@angular/core/testing';

import { Pd3PensionsService } from './pd3-pensions.service';

describe('Pd3PensionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pd3PensionsService = TestBed.get(Pd3PensionsService);
    expect(service).toBeTruthy();
  });
});
