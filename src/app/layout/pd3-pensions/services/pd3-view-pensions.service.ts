import { Injectable } from '@angular/core';
import { API } from 'src/app/http/api';
import { ConfigService } from 'src/app/services/config.service';
import { SMSModel } from 'src/app/models/sms.model';

@Injectable({
  providedIn: 'root'
})
export class Pd3ViewPensionsService {

  constructor(
    private api: API,
    private config: ConfigService
  ) { }

  getPd3ViewPensions(refNo: number) {
    return this.config.getWithToken(this.api.PD3_VIEW_PENSIONS(refNo));
  }


}
