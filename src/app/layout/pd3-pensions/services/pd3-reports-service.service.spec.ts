import { TestBed } from '@angular/core/testing';

import { Pd3ReportsServiceService } from './pd3-reports-service.service';

describe('Pd3ReportsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pd3ReportsServiceService = TestBed.get(Pd3ReportsServiceService);
    expect(service).toBeTruthy();
  });
});
