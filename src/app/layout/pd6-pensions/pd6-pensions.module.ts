import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Pd6PensionsRoutingModule } from './pd6-pensions-routing.module';
import { SearchNicComponent } from './components/search-nic/search-nic.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { Pd6DetailedViewComponent } from './components/pd6-detailed-view/pd6-detailed-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatDialogModule } from '@angular/material';
import { ViewPersonalInfoComponent } from './components/pd6-detailed-view/view-personal-info/view-personal-info.component';
import { ViewPaymentInfoComponent } from './components/pd6-detailed-view/view-payment-info/view-payment-info.component';
import { ConfirmationPopupComponent } from './components/popups/confirmation-popup/confirmation-popup.component';
import { Pd6DashboardComponent } from './components/pd6-dashboard/pd6-dashboard.component';
import { SourceDocumentComponent } from './components/pd6-detailed-view/source-document/source-document.component';
import { Pd6DashboardTileComponent } from './components/pd6-dashboard-tile/pd6-dashboard-tile.component';
import { Pd6RejectedComponent } from './components/pd6-dashboard/pd6-rejected/pd6-rejected.component';
import { Pd6PendingComponent } from './components/pd6-dashboard/pd6-pending/pd6-pending.component';
import { Pd6ApprovedComponent } from './components/pd6-dashboard/pd6-approved/pd6-approved.component';
import { RejectPopupComponent } from './components/popups/reject-popup/reject-popup.component';
import { Pd6AwardComponent } from './components/pd6-dashboard/pd6-award/pd6-award.component';
import { ApprovePopupComponent } from './components/popups/approve-popup/approve-popup.component';
import { ReportComponent } from './components/report/report.component';
import { Pd6NegativeComponent } from './components/pd6-dashboard/pd6-negative/pd6-negative.component';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@NgModule({
  declarations: [SearchNicComponent, Pd6DetailedViewComponent, ViewPersonalInfoComponent, ViewPaymentInfoComponent, ConfirmationPopupComponent, Pd6DashboardComponent, SourceDocumentComponent, Pd6DashboardTileComponent, Pd6RejectedComponent, Pd6PendingComponent, Pd6ApprovedComponent, RejectPopupComponent, Pd6AwardComponent, ApprovePopupComponent, ReportComponent, Pd6NegativeComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    Pd6PensionsRoutingModule,
    MatButtonModule,
    MatTabsModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule
  ],
  entryComponents: [
    ConfirmationPopupComponent,
    RejectPopupComponent,
    ApprovePopupComponent
  ]
})
export class Pd6PensionsModule { }
