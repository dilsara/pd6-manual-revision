import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PensionerService } from '../../../services/pensioner.service';
import { DataConfigService } from '../../../dataconfig/data-config.service';
import { PensionerModel } from '../../../models/pensioner.model';
import { PensionModel } from '../../../models/pension.model';
import { FillPaymentModel } from '../../../models/fillpayment.model';
import { forEach } from '@angular/router/src/utils/collection';
import { element } from '@angular/core/src/render3';
import { NotificationsService } from 'src/app/services/notifications.service';

/**
 * subpaths
 * @author Shageesha Prabagaran
 * @editor Dilsara
 */

@Component({
	selector: 'app-view-payment-info',
	templateUrl: './view-payment-info.component.html',
	styleUrls: ['./view-payment-info.component.scss']
})
export class ViewPaymentInfoComponent implements OnInit {

	form: FormGroup;
	pensiondetail: PensionerModel;
	type = ""; offeredTo: any;
	percentage = [];
	reducedCal: number = null;
	unreducedCal: number = null;
	reduced_perc = null;
	unreduced_perc = null;
	scalename = "";
	gradename = "";
	basic_rev = null;
	rev_reduce_perc = null;
	rev_unreduce_perc = null;
	rev_reduce_salary: number = 0;
	rev_unreduce_salary: number = 0;
	current_annual_consolidated_salary = null;
	cuurent_gross_salary = null;
	sequenceNumber = 1;
	allowance_description;
	nonallowance_description;
	nonallowance_initialamount;
	allowance_amount: number = 0;
	nonallowance_amount: number = 0;
	allarray = [];
	nonallarray = [];
	fixcurrentincre = null;
	fixrevincre = null;
	fixreducedsal = null;
	fixunreducedsal = null;
	fixedunrevreduced = null;
	fixedrevreduced = null;
	today: number = 0;
	difference: number = 0;
	incre_date: number = 0;
	totalpensiontobepaid = null;
	todayfix: number = 0;
	unreduce: number = 0;
	rdatenew: any; salaryincrease: any; particular_year: any; particular_month: any;
	gap: number = 0; newreduced: any; date2: any; date1: any; diff: any; cstate: any;
	ptSorted: any; bpenVariance: any; reducedSal2017Fixed: any;


	fillpayment: FillPaymentModel = {
		circular: "",
		circular_revised: "",
		salary_scale: null,
		service_grade: null,
		basic_salary: null,
		reduce_percentage_revised: null,
		unreduced_percentage_revised: null,
		reduce_percentage_2020: null,
		unreduced_percentage_2020: null,
		increment_date: "",
		reduced_salary: null,
		unreduced_salary: null,
		servicperiod_years: 0,
		servicperiod_months: 0,
		servicperiod_days: 0,
		nopayperiod_years: 0,
		nopayperiod_months: 0,
		nopayperiod_days: 0,
		allowance_desc: "",
		allowance_amount: null,
		allowance_tot: 0,
		nonallowance_tot: 0,
		section: "",
		increment2015: null,
		increment2017: null
	};

	public scalelist: Array<any> = [
		{ "id": "1", "name": "DOR(temp.)" },
		{ "id": "2", "name": "ge-1-2006" },
		{ "id": "3", "name": "ge-1" },
		{ "id": "4", "name": "ge-2-2006" },
		{ "id": "5", "name": "ge-2" },
		{ "id": "6", "name": "ge-4-2006" },
		{ "id": "7", "name": "ge-4" },
		{ "id": "8", "name": "js-1-highcourt" },
		{ "id": "9", "name": "js-4" },
		{ "id": "10", "name": "mn-1" },
		{ "id": "11", "name": "mn-2" },
		{ "id": "12", "name": "mn-3" },
		{ "id": "13", "name": "mn-4" },
		{ "id": "14", "name": "mn-5" },
		{ "id": "15", "name": "mn-6" },
		{ "id": "16", "name": "mn-7" },
		{ "id": "17", "name": "mp-1" },
		{ "id": "18", "name": "mp-2" },
		{ "id": "19", "name": "mt-1" },
		{ "id": "20", "name": "mt-2" },
		{ "id": "21", "name": "mt-3" },
		{ "id": "22", "name": "mt-4" },
		{ "id": "23", "name": "mt-5" },
		{ "id": "24", "name": "mt-6" },
		{ "id": "25", "name": "mt-7" },
		{ "id": "26", "name": "mt-8" },
		{ "id": "27", "name": "or-1-group-I" },
		{ "id": "28", "name": "or-1-group-II" },
		{ "id": "29", "name": "or-1-group-III" },
		{ "id": "30", "name": "or-2-group-I" },
		{ "id": "31", "name": "or-2-group-II" },
		{ "id": "32", "name": "or-2-group-III" },
		{ "id": "33", "name": "or-2-group-sp" },
		{ "id": "34", "name": "or-3-group-I" },
		{ "id": "35", "name": "or-3-group-II" },
		{ "id": "36", "name": "or-3-group-III" },
		{ "id": "37", "name": "or-3-group-sp" },
		{ "id": "38", "name": "or-4-group-I" },
		{ "id": "39", "name": "or-4-group-II" },
		{ "id": "40", "name": "or-4-group-III" },
		{ "id": "41", "name": "or-4-group-sp" },
		{ "id": "42", "name": "or-5-group-I" },
		{ "id": "43", "name": "or-5-group-II" },
		{ "id": "44", "name": "or-5-group-III" },
		{ "id": "45", "name": "or-5-group-sp" },
		{ "id": "46", "name": "or-6-group-I" },
		{ "id": "47", "name": "or-6-group-II" },
		{ "id": "48", "name": "or-6-group-III" },
		{ "id": "49", "name": "or-6-group-sp" },
		{ "id": "50", "name": "or-6a-group-II" },
		{ "id": "51", "name": "or-6a-group-III" },
		{ "id": "52", "name": "or-6a-group-sp" },
		{ "id": "53", "name": "or-7-group-I" },
		{ "id": "54", "name": "or-7-group-II" },
		{ "id": "55", "name": "or-7-group-III" },
		{ "id": "56", "name": "or-7-group-sp" },
		{ "id": "57", "name": "parliament-2006-a" },
		{ "id": "58", "name": "parliament" },
		{ "id": "59", "name": "pl-1" },
		{ "id": "60", "name": "pl-2" },
		{ "id": "61", "name": "pl-3" },
		{ "id": "62", "name": "r-1" },
		{ "id": "63", "name": "r-10" },
		{ "id": "64", "name": "r-2" },
		{ "id": "65", "name": "r-3" },
		{ "id": "66", "name": "r-4" },
		{ "id": "67", "name": "r-5" },
		{ "id": "68", "name": "r-6" },
		{ "id": "69", "name": "r-7" },
		{ "id": "70", "name": "r-8" },
		{ "id": "71", "name": "r-9" },
		{ "id": "72", "name": "rs-1-2006-a" },
		{ "id": "73", "name": "rs-1" },
		{ "id": "74", "name": "rs-2-2006-a" },
		{ "id": "75", "name": "rs-2" },
		{ "id": "76", "name": "rs-3" },
		{ "id": "77", "name": "rs-4" },
		{ "id": "78", "name": "rs-5" },
		{ "id": "79", "name": "sf-1" },
		{ "id": "80", "name": "sf-2" },
		{ "id": "81", "name": "sf-3" },
		{ "id": "82", "name": "sf-4" },
		{ "id": "83", "name": "sl-1" },
		{ "id": "84", "name": "sl-2" },
		{ "id": "85", "name": "sl-3" },
		{ "id": "86", "name": "sl-4" },
		{ "id": "87", "name": "sl-5" },
		{ "id": "98", "name": "sl-6" },
		{ "id": "94", "name": "ts-2-2006-a" },
		{ "id": "95", "name": "ts-1-2006-a" },
		{ "id": "96", "name": "ts-3-2006-a-IV" },
		{ "id": "97", "name": "mn-1-2006-a" },
		{ "id": "99", "name": "sl-7" },
		{ "id": "100", "name": "sl-8" },
		{ "id": "101", "name": "pl-1-2006" },
		{ "id": "102", "name": "pl-3-2006" },
		{ "id": "103", "name": "q-private-secretary" },
		{ "id": "104", "name": "mn-6-2006" },
		{ "id": "105", "name": "rs-1-2006" },
		{ "id": "106", "name": "ts-2-2006" },
		{ "id": "107", "name": "js-2-highcourt" },
		{ "id": "108", "name": "js-2" },
		{ "id": "109", "name": "js-5-2016" },
		{ "id": "110", "name": "q-coordinating-secretary" },
		{ "id": "111", "name": "js-2-2016" },
		{ "id": "112", "name": "js-3-2016" },
		{ "id": "113", "name": "js-4-2016" },
		{ "id": "114", "name": "mt-2-2006" },
		{ "id": "115", "name": "ts-1-2006" },
		{ "id": "116", "name": "sl-2-2006" },
		{ "id": "117", "name": "ts-3-2006" },
		{ "id": "118", "name": "mn-1-2006" },
		{ "id": "119", "name": "mn-2-2006" },
		{ "id": "120", "name": "mn-3-2006" },
		{ "id": "121", "name": "mn-4-2006" },
		{ "id": "122", "name": "mt-1-2006" },
		{ "id": "123", "name": "sl-3-2006" },
		{ "id": "124", "name": "mt-3-2006" },
		{ "id": "125", "name": "mt-4-2006" },
		{ "id": "126", "name": "mt-5-2006" },
		{ "id": "127", "name": "mt-6-2006" },
		{ "id": "128", "name": "mt-7-2006" },
		{ "id": "129", "name": "mt-8-2006" },
		{ "id": "130", "name": "mp-1-2006" },
		{ "id": "131", "name": "mp-2-2006" },
		{ "id": "132", "name": "mn-5-2006" },
		{ "id": "133", "name": "sl-5-2006" },
		{ "id": "134", "name": "mn-7-2006" },
		{ "id": "135", "name": "sf-1-2006" },
		{ "id": "136", "name": "sf-2-2006" },
		{ "id": "137", "name": "sf-3-2006" },
		{ "id": "138", "name": "sf-4-2006" }
	]

	public gradelist: Array<any> = [
		{ "id": "1", "name": "grade-3-Ia" },
		{ "id": "2", "name": "grade-3-Ib" },
		{ "id": "3", "name": "grade-3-Ic" },
		{ "id": "4", "name": "grade-3-II" },
		{ "id": "5", "name": "grade-2-I" },
		{ "id": "6", "name": "grade-2-II" },
		{ "id": "7", "name": "grade-I" },
		{ "id": "8", "name": "grade-II" },
		{ "id": "9", "name": "grade-I-postal" },
		{ "id": "10", "name": "grade-II-postal" },
		{ "id": "11", "name": "special-grade-postal" },
		{ "id": "12", "name": "grade-III" },
		{ "id": "13", "name": "special-grade" },
		{ "id": "14", "name": "pc" },
		{ "id": "15", "name": "ps" },
		{ "id": "16", "name": "si" },
		{ "id": "17", "name": "sm" },
		{ "id": "18", "name": "ip" },
		{ "id": "19", "name": "cip" },
		{ "id": "20", "name": "dentist" },
		{ "id": "21", "name": "medical-officer-grade-I" },
		{ "id": "22", "name": "medical-officer-grade-II" },
		{ "id": "23", "name": "preliminary-grade" },
		{ "id": "24", "name": "adg" },
		{ "id": "25", "name": "adl" },
		{ "id": "26", "name": "ald" },
		{ "id": "27", "name": "asa" },
		{ "id": "28", "name": "ddg" },
		{ "id": "29", "name": "dld" },
		{ "id": "30", "name": "dsg" },
		{ "id": "31", "name": "sa" },
		{ "id": "32", "name": "sald" },
		{ "id": "33", "name": "sasa" },
		{ "id": "34", "name": "sc" },
		{ "id": "35", "name": "ssc" },
		{ "id": "36", "name": "group-I" },
		{ "id": "37", "name": "group-II" },
		{ "id": "38", "name": "group-III" },
		{ "id": "39", "name": "group-sp" },
		{ "id": "40", "name": "ps-grade-II" },
		{ "id": "41", "name": "pc-grade-III" },
		{ "id": "42", "name": "slts-3-II" },
		{ "id": "43", "name": "graduate-teacher" },
		{ "id": "44", "name": "slts-1" },
		{ "id": "45", "name": "slts-2-I" },
		{ "id": "46", "name": "slts-2-II" },
		{ "id": "47", "name": "slts-3-I" },
		{ "id": "48", "name": "trained-teacher" },
		{ "id": "49", "name": "slps-1" },
		{ "id": "50", "name": "slps-2-II" },
		{ "id": "51", "name": "slps-2-I" },
		{ "id": "52", "name": "slps-3" },
		{ "id": "53", "name": "A" },
		{ "id": "54", "name": "B" },
		{ "id": "55", "name": "C" },
		{ "id": "56", "name": "D" },
		{ "id": "57", "name": "E" },
		{ "id": "58", "name": "F" },
		{ "id": "59", "name": "G" },
		{ "id": "60", "name": "H" },
		{ "id": "61", "name": "I" },
		{ "id": "62", "name": "IA" },
		{ "id": "63", "name": "J" },
		{ "id": "64", "name": "K" },
		{ "id": "65", "name": "L" },
		{ "id": "66", "name": "M" },
		{ "id": "67", "name": "N" },
		{ "id": "68", "name": "supra" },
		{ "id": "69", "name": "MO-specialists" },
		{ "id": "70", "name": "Senior-Executive" },
		{ "id": "71", "name": "slts-3-Ia" },
		{ "id": "72", "name": "slts-3-Ib" },
		{ "id": "73", "name": "slts-3-Ic" },
		{ "id": "74", "name": "slts-3-II" },
		{ "id": "75", "name": "slts-2-I" },
		{ "id": "76", "name": "slts-2-II" },
		{ "id": "77", "name": "slts-I" },
		{ "id": "78", "name": "slps-2" },
		{ "id": "79", "name": "chief-custom-inspect" },
		{ "id": "80", "name": "custom-inspect-II" },
		{ "id": "81", "name": "custom-inspect-II" },
		{ "id": "82", "name": "custom-inspect-I" },
		{ "id": "83", "name": "no-grade" },
		{ "id": "84", "name": "grade-1-II" }
	];

	public allowancelist: Array<any> = [
		{ "allowance_id": "3", "special": "0", "description": "Personal Allowances  25%", "type": "allowance" },
		{ "allowance_id": "5", "special": "0", "description": "Pensionable allowance for the postal employee", "type": "earned increment" },
		{ "allowance_id": "6", "special": "0", "description": "Personal Allowances 50%", "type": "allowance" },
		{ "allowance_id": "8", "special": "0", "description": "Pensionable allowances for  the postal employees", "type": "allowance" },
		{ "allowance_id": "9", "special": "0", "description": "Pensionable allowances for  the doctors", "type": "allowance" },
		{ "allowance_id": "20", "special": "0", "description": "Adjustable Allowances", "type": "allowance" },
		{ "allowance_id": "26", "special": "0", "description": "Ration Allowance", "type": "allowance" },
		{ "allowance_id": "27", "special": "0", "description": "Qualification Pay", "type": "allowance" },
		{ "allowance_id": "28", "special": "0", "description": "Re-engagement Pay", "type": "allowance" },
		{ "allowance_id": "7", "special": "0", "description": "Good Conduct", "type": "allowance" }
	];

	public sectionlist: Array<any> = [
		{ "name": "Schedule Q" },
		{ "name": "2:7" },
		{ "name": "2:12" },
		{ "name": "2:14" },
		{ "name": "2-14(30/88)" },
		{ "name": "2:15" },
		{ "name": "2:17" },
		{ "name": "2:17 - 26(14)" },
		{ "name": "2:17 (Not concern to Gratuity)" },
		{ "name": "2:25" },
		{ "name": "Section - 2(b)" },
		{ "name": "W&OP 50%" },
		{ "name": "Orphans(more than 1 child)" },
		{ "name": "WNOP - Death Gratuity" },
		{ "name": "LGS" },
		{ "name": "Section 2:14 (7/2004)" },
		{ "name": "Public Administration Circular 44/90" }
	];

	constructor(private _formBuilder: FormBuilder,
		private router: Router,
		public pensionerService: PensionerService,
		public dataConfig: DataConfigService,
		private notification: NotificationsService) { }


	ngOnInit() {
		this.cstate = false;
		this.fillpayment.salary_scale = 0;
		this.fillpayment.service_grade = 0;
		this.allowance_description = "";
		this.nonallowance_description = "";
		this.nonallowance_initialamount = 0;
		this.fillpayment = this.pensionerService.fillpaymentmodel;
		this.allarray = this.pensionerService.allowancesarraylist;
		this.nonallarray = this.pensionerService.nonallowancesarraylist;

		this.form = this._formBuilder.group({
			paymentInformation: this._formBuilder.group({
				circular: ['', Validators.required],
				reduced_salary: ['', Validators.required],
				unreduced_salary: ['', Validators.required],
				allowance: ['', Validators.required],
				allowance_amount: ['', Validators.required],
				nonallowance_amountt: ['', Validators.required],
				scale: ['', Validators.required],
				grade: ['', Validators.required],
				salary: ['', Validators.required],
				dateofretire: ['', Validators.required],
				dateofincre: ['', Validators.required],
				servicperiod_years: ['', Validators.required],
				servicperiod_months: ['', Validators.required],
				servicperiod_days: ['', Validators.required],
				nopayperiod_years: ['', Validators.required],
				nopayperiod_months: ['', Validators.required],
				nopayperiod_days: ['', Validators.required],
				totallowances: ['', Validators.required],
				totnonallowances: ['', Validators.required],
				non_allowance: ['', Validators.required],
				nonallowance_amount: ['', Validators.required],
				section: ['', Validators.required],
				reduced_2015: ['', Validators.required],
				unreduced_2015: ['', Validators.required],
				reduced_2017: ['', Validators.required],
				unreduced_2017: ['', Validators.required],
			})
		});

		//Re-calculate if any changes done to the form
		this.form.valueChanges.subscribe(value => {
			this.pensionerService.calculateState = false;
		})

		this.fillpayment.circular_revised = "2017";

		if (this.pensionerService.editstate == false) {
			this.fillpayment.circular = null;
			this.fillpayment.section = null;
			this.fillpayment.salary_scale = null;
			this.fillpayment.service_grade = null;
			this.fillpayment.basic_salary = 0;
			this.fillpayment.nopayperiod_days = 0;
			this.fillpayment.nopayperiod_months = 0;
			this.fillpayment.nopayperiod_years = 0;
			this.fillpayment.servicperiod_days = 0;
			this.fillpayment.servicperiod_months = 0;
			this.fillpayment.servicperiod_years = 0;
			this.fillpayment.allowance_tot = 0;
			this.fillpayment.nonallowance_tot = 0;
		}

		this.setdata();
	}

	ngOnDestroy() {
		this.fillpayment.salary_scale = null;
		this.fillpayment.basic_salary = null;
		this.fillpayment.circular = " ";
		this.fillpayment.service_grade = null;
		this.fillpayment.increment_date = " ";
		this.fillpayment.nopayperiod_days = null;
		this.fillpayment.nopayperiod_months = null;
		this.fillpayment.nopayperiod_years = null;
		this.fillpayment.servicperiod_days = null;
		this.fillpayment.servicperiod_months = null;
		this.fillpayment.servicperiod_years = null;
		this.fillpayment.section = "";
		this.allarray = [];
		this.nonallarray = [];
		this.pensionerService.allowancesarraylist = [];
		this.pensionerService.nonallowancesarraylist = [];
		this.fillpayment.allowance_tot = null;
		this.fillpayment.nonallowance_tot = null;
	}

	setdata() {
		this.pensiondetail = this.pensionerService.pensionermodel;

		if (this.pensionerService.editstate == false) {
			this.fillpayment.increment_date = this.pensiondetail.rdate;
		}
	}

	initFormChnageHooks() {
		console.log(this.form.get);
	}

	//<---Allowances calculation - Start---->
	//Set pensionable allowances
	setallowances() {
		// console.log("intial", this.pensionerService.allowancesarraylist);

		this.pensionerService.allowancesarraylist.push({
			id: this.sequenceNumber,
			description: this.allowance_description,
			amount: this.allowance_amount,
			seqNo: this.sequenceNumber,
			status: "",
			is_pensionable: true
		});

		this.allarray = this.pensionerService.allowancesarraylist;
		this.sequenceNumber += 1;
		this.allowance_description = "";
		this.allowance_amount = 0;
		this.totalAllowances();
	}

	//Remove Pensionable Allowances
	remove_allo(index: number) {
		this.allarray = this.pensionerService.allowancesarraylist.filter(el => el.id != index);
		this.sequenceNumber = this.pensionerService.allowancesarraylist.length + 1;
		for (let i = 0; i < this.pensionerService.allowancesarraylist.length; i++) {
			this.pensionerService.allowancesarraylist[i].id = i - 1;

			var y: number = 0;
			y = this.pensionerService.allowancesarraylist[i].amount;
		}
		// console.log("removed amount", y);
		this.fillpayment.allowance_tot = this.fillpayment.allowance_tot - y || 0;
		// console.log("amount after removing", this.fillpayment.allowance_tot);
		this.pensionerService.allowancesarraylist = [];
	}

	//Set Non pensionable allowances
	setnonallowances() {
		this.pensionerService.nonallowancesarraylist.push({
			id: this.sequenceNumber,
			description: this.nonallowance_description,
			amount: this.nonallowance_initialamount,
			seqNo: this.sequenceNumber,
			status: "",
			is_pensionable: false
		});

		this.nonallarray = this.pensionerService.nonallowancesarraylist;
		// console.log("set non allowances", this.nonallarray);
		this.sequenceNumber += 1;
		this.nonallowance_description = "";
		this.nonallowance_initialamount = 0;
		this.totalNonAllowances();
	}

	//Remove non pensionable allowances
	remove_nonallo(index: number) {
		this.nonallarray = this.pensionerService.nonallowancesarraylist.filter(el => el.id != index);
		// this.sequenceNumber = this.pensionerService.nonallowancesarraylist.length + 1;
		for (let i = 0; i < this.pensionerService.nonallowancesarraylist.length; i++) {
			// console.log(this.pensionerService.nonallowancesarraylist.length);
			this.pensionerService.nonallowancesarraylist[i].id = i - 1;
			var x: number = 0;
			x = this.pensionerService.nonallowancesarraylist[i].amount;
		}
		// console.log("removed amount", x);
		this.fillpayment.nonallowance_tot = this.fillpayment.nonallowance_tot - x || 0;
		// console.log("amount after removing", this.fillpayment.nonallowance_tot);
		this.pensionerService.nonallowancesarraylist = [];
	}

	//Total Pensionable Allowances
	totalAllowances() {
		this.fillpayment.allowance_tot = 0;
		this.pensionerService.allowancesarraylist.forEach(element => {
			this.fillpayment.allowance_tot += parseFloat(element.amount);
		})
		// console.log("allowance total", this.fillpayment.allowance_tot);
	}

	//Total non-pensionable Allowances
	totalNonAllowances() {
		this.fillpayment.nonallowance_tot = 0;
		this.pensionerService.nonallowancesarraylist.forEach(element => {
			this.fillpayment.nonallowance_tot += parseFloat(element.amount);
		})
		// console.log("non allowance total", this.fillpayment.nonallowance_tot);
	}

	//<---Allowances calculation - End---->

	getPercentage() {
		//Filter scalename and gradelist for the service
		this.scalelist.forEach(element => {
			if (this.fillpayment.salary_scale == element.id) {
				this.scalename = element.name;
			}
		})

		this.gradelist.forEach(grade => {
			if (this.fillpayment.service_grade == grade.id) {
				this.gradename = grade.name;
			}
		})

		if (this.pensionerService.editstate == false) {
			var dateincrement = this.pensiondetail.rdate;
		} else {
			var dateincrement = this.fillpayment.increment_date;
		}

		//Get the actual bpen 148%
		this.bpenVariance = (this.pensiondetail.bpen * 148) / 100;
		console.log("bpen variance", this.bpenVariance);

		//<-------- REVISED PENSION CALCULATION - PERCENTAGE ----------->
		this.pensionerService.getpensioncalculation(this.scalename, this.gradename, this.fillpayment.basic_salary, this.fillpayment.circular, this.pensiondetail.rdate, dateincrement, this.fillpayment.circular_revised)
			.subscribe(data => {
				// this.getCivilAndMil(data);
				this.getMilPercentage(data);
			}, error => {
				console.log(error);
				if (error["error"]["message"] == "Salary you provided seems to be invalid") {
					this.notification.openSnackBar("Salary you provided seems to be INVALID. Please check the salary and Click CALCULATE.!", '', '');
					this.clearFields();
				}
				else if (error["error"]["code"] == 403 || error["error"]["code"] == 404) {
					console.log("hiii");
					this.notification.openSnackBar("INVALID DATA. Please Re-Check the Circular , Salary Scale , Grade entered to proceed.!", '', '');
					this.clearFields();
				}
			})
	}

	getMilPercentage(data) {
		//Retreieve BASIC SALARY , FIRST ALLOWANCES, SECOND ALLOWANCES 
		console.log("basic sal and increments", data);
		let rev_array = JSON.parse(JSON.stringify(data));

		this.basic_rev = rev_array.basicSalary; //Basic salary for 2017

		//Earn Increment for 2015
		let curentIncrement = rev_array.firstAllowance; //With more decimal points for the calculation
		this.fillpayment.increment2015 = (curentIncrement).toFixed(2);
		this.fixcurrentincre = (curentIncrement / 12.0).toFixed(2); //toFixed(2)

		//Earn increment for 2017
		let revIncrement = rev_array.secondAllowance;
		this.fillpayment.increment2017 = (revIncrement).toFixed(2);
		this.fixrevincre = (revIncrement / 12.0).toFixed(2);

		let revAnnualConsolidatedSalary = this.basic_rev * 12;
		let revGrossAnnualSalary = revAnnualConsolidatedSalary + revIncrement + this.fillpayment.allowance_tot;
		console.log("Gross salary used for 2017", revGrossAnnualSalary);

		let widowPensionList: Array<any> = [1, 2, 13, 14, 15, 21, 22, 24, 25, 27, 28, 31, 33, 40, 43, 44, 45, 60, 61, 62, 63]

		widowPensionList.forEach(pt => {
			if (this.pensionerService.pensionermodel.pt == pt) {
				this.ptSorted = pt;
			}
		});
		console.log(this.ptSorted);

		if (this.pensionerService.pensionermodel.pt == this.ptSorted) {
			this.type = "civil";

			//<-------- 2017 CALCULATION MILITORY----------->
			this.pensionerService.getpercentage(this.type, this.fillpayment.servicperiod_months,
				revAnnualConsolidatedSalary, this.fillpayment.circular_revised, this.fillpayment.servicperiod_years)
				.subscribe(data => {
					console.log(data);
					//Retreive circular, reduced, unreduced details
					let revPercentageMilitory = JSON.parse(JSON.stringify(data));
					this.rev_reduce_perc = revPercentageMilitory.reduced;
					this.rev_unreduce_perc = revPercentageMilitory.unreduced;
				}, error => {
					if (error["error"]["code"] == 404) {
						this.rev_reduce_perc = 0;
						this.rev_unreduce_perc = 0;
					}
					// this.notification.openSnackBar(error["error"]["message"], '', '');
				})

			//<--------- 2006 CALCULATION MILITORY-------------> 
			this.current_annual_consolidated_salary = this.fillpayment.basic_salary * 12;
			this.cuurent_gross_salary = (this.current_annual_consolidated_salary + curentIncrement + this.fillpayment.allowance_tot);

			this.pensionerService.getpercentage(this.type, this.fillpayment.servicperiod_months, this.current_annual_consolidated_salary,
				this.fillpayment.circular, this.fillpayment.servicperiod_years)
				.subscribe(data => {
					console.log(data);
					this.percentage = [JSON.parse(JSON.stringify(data))];
					// this.change1();
					this.reduced_perc = this.percentage[0].reduced;
					this.unreduced_perc = this.percentage[0].unreduced;
				}, error => {
					if (error["error"]["code"] == 404) {
						this.reduced_perc = 0;
						this.unreduced_perc = 0;
					}
				})
		}

		this.notification.openSnackBar("View Percentage details ! NOW PRESS CALCULATE BUTTON !", '', '');
	}

	getFinalCal() {

		this.cstate = true;

		if (this.pensionerService.editstate == false) {
			var dateincrement = this.pensiondetail.rdate;
		} else {
			var dateincrement = this.fillpayment.increment_date;
		}

		this.pensionerService.getpensioncalculation(this.scalename, this.gradename, this.fillpayment.basic_salary, this.fillpayment.circular, this.pensiondetail.rdate, dateincrement, this.fillpayment.circular_revised)
			.subscribe(data => {

				console.log("basic sal and increments", data);
				let rev_array = JSON.parse(JSON.stringify(data));

				this.basic_rev = rev_array.basicSalary; //Basic salary for 2017

				//Earn Increment for 2015
				let curentIncrement = rev_array.firstAllowance; //With more decimal points for the calculation
				this.fillpayment.increment2015 = (curentIncrement).toFixed(2);
				this.fixcurrentincre = (curentIncrement / 12.0).toFixed(2); //toFixed(2)

				//Earn increment for 2017
				let revIncrement = rev_array.secondAllowance;
				this.fillpayment.increment2017 = (revIncrement).toFixed(2);
				this.fixrevincre = (revIncrement / 12.0).toFixed(2);

				let revAnnualConsolidatedSalary = this.basic_rev * 12;
				let revGrossAnnualSalary = revAnnualConsolidatedSalary + revIncrement + this.fillpayment.allowance_tot;
				console.log("Gross salary used for 2017", revGrossAnnualSalary);

				//2017 reduced salary Start
				let reducedSal2017 = (revGrossAnnualSalary * this.rev_reduce_perc / 100) / 12;
				this.reducedSal2017Fixed = (reducedSal2017).toFixed(2);
				console.log("final reduced sal for the variance", this.reducedSal2017Fixed);

				//2017 unreduced salary
				let rev_unreduce_salary = revGrossAnnualSalary * this.rev_unreduce_perc / 100;
				this.rev_unreduce_salary = rev_unreduce_salary / 12;
				this.fixedunrevreduced = (this.rev_unreduce_salary).toFixed(2);

				//<---Calculate the sal diffrence = Unreduced sal - reduced sal--->
				var today = new Date();
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
				let today1 = yyyy + '-' + mm + '-' + dd;
				this.todayfix = parseInt(today1.slice(0, 4));

				//<-- If retired date is more than 2017-01-01 prompt a message  START--->
				let incre_date = this.fillpayment.increment_date;
				this.rdatenew = this.pensiondetail.rdate;
				var myDate = new Date(this.rdatenew);
				let result = myDate.getTime();

				var myDate1 = new Date("2017-01-01");
				let result1 = myDate1.getTime();

				if (result > result1) {
					this.notification.openSnackBar("Invalid retired Date", '', '');
				}

				//<--------- 2006 CALCULATION MILITORY-------------> 
				this.current_annual_consolidated_salary = this.fillpayment.basic_salary * 12;
				this.cuurent_gross_salary = (this.current_annual_consolidated_salary + curentIncrement + this.fillpayment.allowance_tot);

				//20006 - reduced salary 
				let reduced_salary = this.cuurent_gross_salary * this.reduced_perc / 100;
				this.reducedCal = reduced_salary / 12;
				this.fixreducedsal = (this.reducedCal).toFixed(2);
				//2006 - unreduced salary
				let unreduced_salary = this.cuurent_gross_salary * this.unreduced_perc / 100;
				this.unreducedCal = unreduced_salary / 12;
				this.fixunreducedsal = (this.unreducedCal).toFixed(2);

				//<---Calculate sal diffrence = 2015 unreduced sal - 2015 reduced sal-->
				this.gap = this.fixunreducedsal - this.fixreducedsal;

				//<---Calculate new reduced salary--->
				//2017 reduced salary
				let v = this.fixedunrevreduced - this.gap
				this.fixedrevreduced = v.toFixed(2);

				this.date2 = new Date(this.rdatenew);
				this.date1 = new Date();
				var x = this.date1 - this.date2;
				this.diff = new Date(x);
				let years = this.diff.toISOString().slice(0, 4) - 1970;
				let months = this.diff.getMonth();
				let days = this.diff.getDate() - 1;
				console.log("Y/M/D", years, months, days);

				if (this.pensionerService.pensionermodel.pt == 21 || this.pensionerService.pensionermodel.pt == 22) {
					this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseInt(this.fillpayment.nonallowance_tot + "") +
						parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
					this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
					this.pensionerService.bpenPension = this.rev_unreduce_salary;
				} else {

					if (years <= 10) {
						if (years == 10) {
							if (months == 0 && days == 0) {
								this.totalpensiontobepaid = (parseFloat(this.fixedrevreduced + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
									parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
								this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
								this.pensionerService.bpenPension = this.reducedSal2017Fixed;
							} else if (months > 0 || days > 0) {
								this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
									parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
								this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
								this.pensionerService.bpenPension = this.rev_unreduce_salary;
							}
						} else if (years < 10) {
							this.totalpensiontobepaid = (parseFloat(this.fixedrevreduced + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
								parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
							this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
							this.pensionerService.bpenPension = this.reducedSal2017Fixed;
						}
					} else if (years > 10) {
						this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
							parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
						this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
						this.pensionerService.bpenPension = this.rev_unreduce_salary;
					}
				}
			})

		this.pensionerService.calculateState = true;
	}

	clearFields() {
		this.reduced_perc = "";
		this.unreduced_perc = "";
		this.fillpayment.increment2017 = null;
		this.fixreducedsal = "";
		this.fixunreducedsal = "";
		this.basic_rev = "";
		this.rev_reduce_perc = "";
		this.rev_unreduce_perc = "";
		this.fillpayment.increment2015 = null;
		this.fixedrevreduced = "";
		this.fixedunrevreduced = "";
		this.totalpensiontobepaid = "";
		this.salaryincrease = "";
	}

}
