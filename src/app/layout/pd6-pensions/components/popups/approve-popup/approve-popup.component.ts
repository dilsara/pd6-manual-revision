import { Component, OnInit } from '@angular/core';
import { PensionerService } from '../../../services/pensioner.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

@Component({
	selector: 'app-approve-popup',
	templateUrl: './approve-popup.component.html',
	styleUrls: ['./approve-popup.component.scss']
})
export class ApprovePopupComponent implements OnInit {
	data: any;

	constructor(private pensionerservice: PensionerService,
		private notification: NotificationsService,
		private router: Router,
		public dialogRef: MatDialogRef<ApprovePopupComponent>) {
		dialogRef.disableClose = true;
	}

	ngOnInit() {
	}

	approve() {
		let user = localStorage.getItem("username");
		let penno = localStorage.getItem("pension_id");
		let refnumber = localStorage.getItem("ref");

		let values = {
			id: parseInt(refnumber),
			status: 300,
			reson: "Approved Application",
			user: user
		}

		this.pensionerservice.change_status(values).subscribe(data => {
			this.data = JSON.parse(JSON.stringify(data));
			if (this.data.code == 200) {
				this.notification.openSnackBar("Application is approved", '', '');
				this.close();
			}
		})
		this.router.navigateByUrl("/login/pd6-pensions");
	}


	close(): void {
		this.dialogRef.close(); //close dialog
	}

}
