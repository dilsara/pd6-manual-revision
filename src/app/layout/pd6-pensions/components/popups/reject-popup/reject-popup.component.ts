import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { Router } from '@angular/router';
import { PensionerService } from '../../../services/pensioner.service';
import { NotificationsService } from 'src/app/services/notifications.service';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

@Component({
	selector: 'app-reject-popup',
	templateUrl: './reject-popup.component.html',
	styleUrls: ['./reject-popup.component.scss']
})
export class RejectPopupComponent implements OnInit {

	reason: any;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		public dialogRef: MatDialogRef<RejectPopupComponent>,
		private router: Router,
		private pensionerservice: PensionerService,
		private notification: NotificationsService) {
		dialogRef.disableClose = true;
	}

	ngOnInit() {
	}

	close(): void {
		this.dialogRef.close();
	}

	submit_reason() {
		let user = localStorage.getItem("username");
		let penno = localStorage.getItem("pension_id");
		let refnumber = localStorage.getItem("ref");

		let values = {
			id: parseInt(refnumber),
			status: 200,
			reson: this.reason,
			user: user
		}

		this.pensionerservice.change_status(values).subscribe(data => {
			this.data = JSON.parse(JSON.stringify(data));
			if (this.data.code == 200) {
				this.notification.openSnackBar("Application is rejected", '', '');
			}

		});
		this.close();
		this.router.navigateByUrl("/login/pd6-pensions");
	}

}
