import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
	selector: 'app-report',
	templateUrl: './report.component.html',
	styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
	public iframeLoaded: boolean = false;
	public iframeURL: SafeResourceUrl;
	public iframeclass="onframeload";
	public loaded = false;
	public dscode;
	public loadedCount = 0;

	public statuslist:Array<any> =[
		{"id":"100","name":"Pending"},
		{"id":"200","name":"Rejected"},
		{"id":"300","name":"Approved"},
	]

	constructor(
		private sanitizer: DomSanitizer
	) { }

	ngOnInit() {
	}

	viewReport() {
		this.iframeLoaded = true;
		$('iframe').attr('src', $('iframe').attr('src'));
		this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(`http://192.168.100.101:81/jasperserver/rest_v2/reports/reports/postal_report.html?SELECTED_DATE=${this.dscode}&j_username=jasperadmin&j_password=root`);
		$('iframe').attr('src', $('iframe').attr('src'));
		this.loaded = true;
	}

	download() {
		window.open(`http://192.168.100.101:81/jasperserver/rest_v2/reports/reports/postal_report.pdf?SELECTED_DATE=${this.dscode}&j_username=jasperadmin&j_password=root`, 'this');
	}

	changeStyle() {
		this.iframeclass = "afterframeload";
	}

	onMyFrameLoad() {
		this.loadedCount += 1;
		if (this.loadedCount == 2) {
			this.changeStyle();
			this.loadedCount = 0;
		}
	}

}
