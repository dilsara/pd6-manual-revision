import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6ApprovedComponent } from './pd6-approved.component';

describe('Pd6ApprovedComponent', () => {
  let component: Pd6ApprovedComponent;
  let fixture: ComponentFixture<Pd6ApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6ApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6ApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
