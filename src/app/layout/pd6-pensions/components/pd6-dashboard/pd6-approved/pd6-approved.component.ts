import { Component, OnInit } from '@angular/core';
import { PensionerService } from '../../../services/pensioner.service';
import { Router } from '@angular/router';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

@Component({
	selector: 'app-pd6-approved',
	templateUrl: './pd6-approved.component.html',
	styleUrls: ['./pd6-approved.component.scss']
})
export class Pd6ApprovedComponent implements OnInit {
	approveddetails: any = []; nic: any;

	constructor(private pensionerservice: PensionerService,
		private router: Router) { }

	ngOnInit() {
		let username = localStorage.getItem("username");

		this.pensionerservice.get_list(username, 300).subscribe(data => {
			this.approveddetails = JSON.parse(JSON.stringify(data));
			console.log(this.approveddetails);
			this.nic = this.approveddetails.nic;

		})
	}

	details(refNumber: string) {
		this.pensionerservice.approvedState = true;
		// console.log(refNumber);
		localStorage.setItem("ref", refNumber);
		this.router.navigateByUrl('/login/pd6-pensions/pd6-sourcedoc');
	}

	searchnic() {
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("nic");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[1];
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}

	view_award(refnumber: string) {
		localStorage.setItem("ref", refnumber);
		this.router.navigateByUrl('/login/pd6-pensions/award');
	}

}
