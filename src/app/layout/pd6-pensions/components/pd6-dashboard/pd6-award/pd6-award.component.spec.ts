import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6AwardComponent } from './pd6-award.component';

describe('Pd6AwardComponent', () => {
  let component: Pd6AwardComponent;
  let fixture: ComponentFixture<Pd6AwardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6AwardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6AwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
