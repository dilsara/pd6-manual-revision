import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PensionerService } from '../../../services/pensioner.service';
import { PensionModel } from '../../../models/pension.model';
import { PensionerModel } from '../../../models/pensioner.model';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

@Component({
	selector: 'app-pd6-rejected',
	templateUrl: './pd6-rejected.component.html',
	styleUrls: ['./pd6-rejected.component.scss']
})
export class Pd6RejectedComponent implements OnInit {
	rejectedddetails: any;
	pensionmodel: PensionModel;
	data: any;
	public pensionermodel: PensionerModel;
	nic: any;
	final: any;
	dscode: any;
	allarray: any = [];
	districtarray: any = [];
	pension_officer: any; accountant: any;
	reasonarray: any = []; arrayx: any = [];

	constructor(private router: Router,
		private pensionerservice: PensionerService) {
		this.pensionermodel = new PensionerModel();
	}


	ngOnInit() {
		let username = localStorage.getItem("username");

		this.pensionerservice.get_list(username, 200).subscribe(data => {
			this.rejectedddetails = JSON.parse(JSON.stringify(data));
			// console.log(this.rejectedddetails[0].rejection_reasons[0].reason);

			this.rejectedddetails.forEach(element => {
				// console.log(element.rejection_reasons);
				this.reasonarray = element.rejection_reasons;
				this.reasonarray.forEach(element => {
					// console.log(element.reason);
					this.arrayx.push({
						reason: element.reason,
						date: element.timeStamp
					})
				});
				// console.log(this.arrayx);
			});
		})

		let userrole = localStorage.getItem("role");
		if (userrole == "DS_PENSION_OFFICER") {
			this.pension_officer = userrole;
		} else if (userrole == "DS_ACCOUNTANT") {
			this.accountant = userrole;
		}
	}

	View(refNumber: string) {
		this.pensionerservice.approvedState = false;
		localStorage.setItem("ref", refNumber);
		this.router.navigateByUrl('/login/pd6-pensions/pd6-sourcedoc');
	}

	Edit(refNumber: string) {
		this.pensionerservice.editstate = true;
		localStorage.setItem("ref", refNumber);
		// console.log(this.pensionerservice.editstate);

		this.pensionerservice.getpd6pensionerdetails(parseInt(refNumber)).subscribe(data => {
			console.log("model for edit", data);

			this.pensionerservice.pensionermodel.name = data["personalInfo"].fullName;
			this.pensionerservice.pensionermodel.id = data["revision"].pid;
			this.pensionerservice.pensionermodel.nic = data["personalInfo"].nic;
			this.pensionerservice.pensionermodel.dob = data["personalInfo"].dob;
			this.pensionerservice.pensionermodel.ad1 = data["personalInfo"].address.addressLine1;
			this.pensionerservice.pensionermodel.ad2 = data["personalInfo"].address.addressLine2;
			this.pensionerservice.pensionermodel.ad3 = data["personalInfo"].address.addressLine3;
			this.pensionerservice.pensionermodel.mobile = data["personalInfo"].mobile;
			this.pensionerservice.pensionermodel.phone = data["personalInfo"].landNumber;
			this.nic = data["personalInfo"].nic;
			this.pensionerservice.pensionermodel.dscode = data["personalInfo"].dscode;
			this.pensionerservice.fillpaymentmodel.circular = data["revision"].prev_circular;
			this.pensionerservice.fillpaymentmodel.salary_scale = data["revision"].salary_scale;
			this.pensionerservice.fillpaymentmodel.service_grade = data["pension_service"].service_grade;
			this.pensionerservice.fillpaymentmodel.basic_salary = data["revision"].prev_basic_salary;
			this.pensionerservice.pensionermodel.rdate = data["pension_service"].retired_date;
			this.pensionerservice.fillpaymentmodel.increment_date = data["pension"].increment_date;
			this.pensionerservice.fillpaymentmodel.servicperiod_days = data["pension_service"].net_service_days;
			this.pensionerservice.fillpaymentmodel.servicperiod_months = data["pension_service"].net_service_months;
			this.pensionerservice.fillpaymentmodel.servicperiod_years = data["pension_service"].net_service_years;
			this.pensionerservice.fillpaymentmodel.nopayperiod_days = data["pension_service"].nopay_days;
			this.pensionerservice.fillpaymentmodel.nopayperiod_months = data["pension_service"].nopay_months;
			this.pensionerservice.fillpaymentmodel.nopayperiod_years = data["pension_service"].nopay_years;
			this.pensionerservice.fillpersonalmodel.service = data["pension_service"].service;
			this.pensionerservice.fillpaymentmodel.section = data["pension_service"].section;
			this.pensionerservice.fillpersonalmodel.gender = data["personalInfo"].gender;
			this.pensionerservice.fillpersonalmodel.designation = data["pension_service"].designation;
			this.pensionerservice.fillpersonalmodel.id = data["personalInfo"].id;
			this.pensionerservice.fillpersonalmodel.title = data["personalInfo"].title;
			this.pensionerservice.fillpersonalmodel.pensionid = data["revision"].pid;
			this.pensionerservice.pt = data["pension"].pension_type;

			localStorage.setItem("pension_id", data["pension"].id);
			let arraydata: any[] = data["allowances"].data;
			// console.log(arraydata); 
			this.pensionerservice.fillpaymentmodel.nonallowance_tot = 0;
			this.pensionerservice.fillpaymentmodel.allowance_tot = 0;

			arraydata.forEach(element => {
				if (element.is_pensionable == false) {
					// this.pensionerservice.nonallowancesarraylist =[];
					this.pensionerservice.nonallowancesarraylist.push(element);
					//console.log("non pensionable allowances", this.pensionerservice.nonallowancesarraylist);

					this.pensionerservice.fillpaymentmodel.nonallowance_tot = 0;
					this.pensionerservice.nonallowancesarraylist.forEach(element => {
						this.pensionerservice.fillpaymentmodel.nonallowance_tot += parseFloat(element.amount);
						//console.log("allowance", this.pensionerservice.fillpaymentmodel.nonallowance_tot);
					})

				}
				else if (element.is_pensionable == true) {
					// this.pensionerservice.allowancesarraylist =[];
					this.pensionerservice.allowancesarraylist.push(element);
					//console.log("pensionable allowances", this.pensionerservice.allowancesarraylist);

					this.pensionerservice.fillpaymentmodel.allowance_tot = 0;
					this.pensionerservice.allowancesarraylist.forEach(element => {
						this.pensionerservice.fillpaymentmodel.allowance_tot += parseFloat(element.amount);
						//console.log("non", this.pensionerservice.fillpaymentmodel.allowance_tot);
					})
				}
			});

			this.pensionerservice.getPensionerByPid(data["revision"].pid).subscribe(data => {
				this.final = JSON.parse(JSON.stringify(data));
				this.pensionerservice.pensionermodel.bpen = this.final.bpen;
				this.pensionerservice.pensionermodel.tpen = this.final.tpen;
				this.pensionerservice.pensionermodel.bac = this.final.bac;
				this.pensionerservice.pensionermodel.brno = this.final.brno;
				this.pensionerservice.pensionermodel.pt = this.final.pt;

				console.log("pension type", this.pensionerservice.pensionermodel.pt);


				// this.pensionerservice.pensionermodel.cla = this.final.cla;

				this.pensionerservice.searchdistrictdetails(this.pensionerservice.pensionermodel.dscode).subscribe(data => {
					this.districtarray = [JSON.parse(JSON.stringify(data))];
					this.pensionerservice.fillpersonalmodel.district = this.districtarray[0].dsOffice.district.name;
					this.pensionerservice.fillpersonalmodel.division = this.districtarray[0].dsOffice.name;
				})
			})

			this.router.navigateByUrl("/login/pd6-pensions/pd6-detailed-view");
		})

	}

	searchnic() {
		// console.log("onkey event");
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("nic");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[1];
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}

	}

}
