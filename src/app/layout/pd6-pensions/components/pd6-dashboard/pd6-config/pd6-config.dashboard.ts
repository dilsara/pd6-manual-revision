export default class PD6DashboardConfig {

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

    dashboardConfig: Object[];

    constructor() {

    }

    getDashboardConfig(userRole: string) {
        if (userRole == "DS_PENSION_OFFICER") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "/login/pd6-pensions/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "/login/pd6-pensions/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Approved",
                    "path": "/login/pd6-pensions/approved",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "DS_ACCOUNTANT") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "/login/pd6-pensions/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "/login/pd6-pensions/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Approved",
                    "path": "/login/pd6-pensions/approved",
                    "color": "#16a085"
                }
            ]
        }
        else if (userRole == "DISTRICT_OFFICER") {
            this.dashboardConfig = [
                {
                    "name": "Pending",
                    "path": "/login/pd6-pensions/pending",
                    "color": "#8e44ad"
                },
                {
                    "name": "Rejected",
                    "path": "/login/pd6-pensions/rejected",
                    "color": "#e74c3c"
                },
                {
                    "name": "Approved",
                    "path": "/login/pd6-pensions/approved",
                    "color": "#16a085"
                }
            ]
        }

    }

}