import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6DashboardTileComponent } from './pd6-dashboard-tile.component';

describe('Pd6DashboardTileComponent', () => {
  let component: Pd6DashboardTileComponent;
  let fixture: ComponentFixture<Pd6DashboardTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6DashboardTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6DashboardTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
