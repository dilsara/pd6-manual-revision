import { Component, OnInit, Input } from '@angular/core';
import { PensionerService } from '../../services/pensioner.service';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-pd6-dashboard-tile',
	templateUrl: './pd6-dashboard-tile.component.html',
	styleUrls: ['./pd6-dashboard-tile.component.scss']
})
export class Pd6DashboardTileComponent implements OnInit {

	@Input() name: string;
	@Input() path: string;
	@Input() color: string;

	approved = "";
	pending = "";
	rejected = "";

	counts = [];

	constructor(
		private pensionerservice: PensionerService
	) {
	}

	ngOnInit() {
		let values = {
			"userName": localStorage.getItem("username"),
			"module": "revision",
			"pending": 100,
			"approved": 300,
			"reject": 200,
			"finalstate": 100
		}

		this.pensionerservice.dashboard_count(values).subscribe(data => {
			// console.log(data);
			this.counts = [JSON.parse(JSON.stringify(data))];
			this.approved = this.counts[0].approved;
			this.pending = this.counts[0].pending;
			this.rejected = this.counts[0].reject;

		})
	}

}
