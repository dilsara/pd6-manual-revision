import { Injectable } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { HttpClient } from '@angular/common/http';
import { PDSIXAPI } from 'src/app/http/pd6-api';
import { API } from 'src/app/http/api';
import { Observable } from 'rxjs';
import { PensionerModel } from '../models/pensioner.model';
import { PensionModel } from "../models/pension.model";
import { SMSModel } from "src/app/models/sms.model";
import { FillPersonalModel } from '../models/fillpersonal.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FillPaymentModel } from '../models/fillpayment.model';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Injectable({
	providedIn:
		'root'
})
export class PensionerService {

	pensionermodel: PensionerModel;
	fillpersonalmodel: FillPersonalModel;
	pensionmodel: PensionModel;
	smsmodel: SMSModel;
	fillpaymentmodel: FillPaymentModel;
	allowancesarraylist = [];
	nonallowancesarraylist = [];
	editstate = false;
	calculateState = false;
	alreadyExists = false;
	personId = 0;
	pt = 0;
	claFixedAmount: any = 3525;
	bpenPension: any;
	approvedState = false;

	constructor(
		private api: PDSIXAPI,
		private http: HttpClient,
		private configService: ConfigService,
		private api1: API) {
		this.pensionmodel = new PensionModel();
		this.pensionermodel = new PensionerModel();
		this.fillpaymentmodel = new FillPaymentModel();
		this.fillpersonalmodel = new FillPersonalModel();
	}

	//Search PD6 pensioners using NIC
	searchpd6PensionerByNic(nic: string) {
		return this.configService.getWithToken(this.api.PD6_SEARCH_PENSIONER_BY_NIC(nic));
	}

	//Send pd6 pensioner details to the database
	sendpd6PensionDetails(json: PensionModel): Observable<any> {
		return this.configService.postWithToken(this.api1.BASE_PATH1 + '/pd6-revision', json);
	}

	//Retrieve district details using dscode
	searchdistrictdetails(dscode: number) {
		return this.configService.get(this.api.GET_DISTRCIT_DETAILS(dscode));
	}

	//Revision Calculation
	getpensioncalculation(scale: String, grade: String, salary: number, circular: String, retired_date: String, increment_date: String, target: String) {
		return this.configService.get(this.api.GET_CALCULATION(scale, grade, salary, circular, retired_date, increment_date, target))
	}

	//Get percentage - Civil
	getpercentage(type: String, month: number, salary: number, circular: String, years: number) {
		return this.configService.get(this.api.GET_PERCENTAGE(type, month, salary, circular, years));
	}

	//Get percentage - Militory
	getpercentagemilitory(type: String, month: number, salary: number, circular: String, years: number, offeredto: String) {
		return this.configService.get(this.api.GET_PERCENTAGE_MILITORY(type, month, salary, circular, years, offeredto));
	}

	//Send SMS
	sendSms(json: SMSModel): Observable<any> {
		return this.http.put<any>(this.api.SEND_SMS(), json);
	}

	//Dashboard counts
	dashboard_count(json: any): Observable<any> {
		return this.configService.postWithToken(this.api.DASHBOARD_COUNTS(), json);
	}

	//Retrieve pensioner details
	getpd6pensionerdetails(ref: number) {
		return this.configService.get(this.api.GET_PD6PENSIONER_DETAILS(ref));
	}

	//NIC checking
	get_nic_details(nic: String) {
		return this.configService.get(this.api.GET_NIC_DETAILS(nic));
	}

	get_list(username: String, state: number) {
		return this.configService.get(this.api.LIST_VIEW(username, state));
	}

	put_pensioner_details(ref: number, json: any) {
		return this.configService.put(this.api.EDIT_PENSIONER_DETAILS(ref), json);
	}

	//Chnage the state
	change_status(json: any) {
		return this.configService.postWithToken(this.api.CHANGE_STATE(), json);
	}

	check_dscode(username: String, nic: String) {
		return this.configService.get(this.api.CHECK_DSCODE(username, nic));
	}

	get_designationlist() {
		return this.configService.get(this.api.GET_DESIGNATIONLIST());
	}

	get_servicelist() {
		return this.configService.get(this.api.GET_SERVICELIST());
	}

	checkNicAvailabilty(nic: String) {
		return this.configService.get(this.api.CHECK_NIC_AVAILABILTY(nic));
	}

	getSavedPersonalInfo(nic: String, pt: number) {
		return this.configService.get(this.api.GET_PEROSNALINFO(nic, pt));
	}

	//Retrieve EDward sir's information for source document adn Edit using PID
	getPensionerByPid(pid: number) {
		return this.configService.getWithToken(this.api.GET_PENSIONER_BYPID(pid));
	}

	getStep(scale: String, grade: String, salary: number, retired_date: String, increment_date: String) {
		return this.configService.get(this.api.GET_STEP(scale, grade, salary, retired_date, increment_date));
	}

	get_new_details(scale: String, grade: String, step: number) {
		return this.configService.get(this.api.GET_NEW_DETAILS(scale, grade, step));
	}

	getJuneRecords(pid: number) {
		return this.configService.getWithToken(this.api.GET_JUNE_RECORDS(pid));
	}
}
