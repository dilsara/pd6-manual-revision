import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchNicComponent } from './components/search-nic/search-nic.component';
import { Pd6DetailedViewComponent } from './components/pd6-detailed-view/pd6-detailed-view.component';
import { Pd6DashboardComponent } from './components/pd6-dashboard/pd6-dashboard.component';
import { SourceDocumentComponent } from './components/pd6-detailed-view/source-document/source-document.component';
import { Pd6PendingComponent } from './components/pd6-dashboard/pd6-pending/pd6-pending.component';
import { Pd6RejectedComponent } from './components/pd6-dashboard/pd6-rejected/pd6-rejected.component';
import { Pd6ApprovedComponent } from './components/pd6-dashboard/pd6-approved/pd6-approved.component';
import { Pd6AwardComponent } from './components/pd6-dashboard/pd6-award/pd6-award.component';
import { ReportComponent } from './components/report/report.component';

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

const routes: Routes = [
  { path: '', component: Pd6DashboardComponent },
  { path: 'search-nic', component: SearchNicComponent },
  { path: 'pd6-detailed-view', component: Pd6DetailedViewComponent },
  { path: 'pd6-sourcedoc', component: SourceDocumentComponent },
  { path: 'pending', component: Pd6PendingComponent },
  { path: 'rejected', component: Pd6RejectedComponent },
  { path: 'approved', component: Pd6ApprovedComponent },
  { path: 'award', component: Pd6AwardComponent },
  { path: 'report', component: ReportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pd6PensionsRoutingModule { }
