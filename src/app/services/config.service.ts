import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Authentication } from '../models/auth.model';

@Injectable({
	providedIn: 'root'
})

export class ConfigService {

	constructor(
		private http: HttpClient
	) { }

	/**
	 * service callers 
	 */

	//http get
	get(url: string) {
		return this.http.get(url);
	}

	//http get with session id
	getWithToken(url: string) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxNjY1IiwiaWF0IjoxNTc5Njc0ODYzLCJzdWIiOiJhdXRoX3Rva2VuIiwiaXNzIjoicGVuc2lvbmRwdCIsImIiOiJ0ZXN0cG8zIiwiYSI6IkRTX1BFTlNJT05fT0ZGSUNFUiIsImQiOjEsImV4cCI6MTU3OTcwMzY2M30.KnUD1FmPcFO_uoUR-T-o19mi9SboHq_Mz4dULpgh5ic"

			})
		};
		return this.http.get(url, httpOptions);
	}

	//http post
	post(url: string, data: any) {
		return this.http.post(url, data);
	}

	//http post with session id
	postWithToken(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMjYiLCJpYXQiOjE1NzA0NDExNTYsInN1YiI6ImF1dGhfdG9rZW4iLCJpc3MiOiJwZW5zaW9uZHB0IiwiYiI6InRlc3RwbzMiLCJhIjoiRFNfUEVOU0lPTl9PRkZJQ0VSIiwiZCI6MSwiZXhwIjoxNTcwNDY5OTU2fQ.AN3YGR6t6jyxtlm0H9RhnvxYNQwMS6pogbT4XWOIYjg"
			})
		};
		return this.http.post(url, data, httpOptions);
	}

	//http put
	put(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMjYiLCJpYXQiOjE1NzA0NDExNTYsInN1YiI6ImF1dGhfdG9rZW4iLCJpc3MiOiJwZW5zaW9uZHB0IiwiYiI6InRlc3RwbzMiLCJhIjoiRFNfUEVOU0lPTl9PRkZJQ0VSIiwiZCI6MSwiZXhwIjoxNTcwNDY5OTU2fQ.AN3YGR6t6jyxtlm0H9RhnvxYNQwMS6pogbT4XWOIYjg"
			})
		};
		return this.http.put(url, data, httpOptions);
	}

	//http delete
	delete(url: string) {
		return this.http.delete(url);
	}


}
