import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { API } from '../http/api';
import { Authentication } from '../models/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  //authentication
  authentication: Authentication = new Authentication();

  constructor(
    private config: ConfigService,
    private path: API
  ) { }


  authenticate() {
    return this.config.post(this.path.AUTHENTICATION, this.authentication);
  }
}
