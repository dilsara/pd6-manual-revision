import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable()
export class LoggerService {

  log(message: any) {
    if (!environment.production) {
        console.log(message);
    } 
  }

}